**zQuill**  
Version 1.4.1  
Auraes, 2021-2025.  
Licence Creative Commons BY-SA 4.0 (CC BY-SA 4.0)  
https://creativecommons.org/licenses/by-sa/4.0/deed.fr

zQuill permet de convertir des jeux d’aventure textuels créés avec le logiciel  
The Quill/Adventure Writer, vers la machine virtuelle Z-machine. Les jeux sont  
décompilés avec le logiciel UnQuill, convertis en langage Inform 6 et jouables  
avec des interpréteurs modernes et multiplateformes tels que Frotz (Windows  
ou Linux). Pour plus d’informations sur le jeu d’instructions, consultez les  
manuels de la version Zx Spectrum et commodore 64 de The Quill.  
Il est aussi possible de créer directement son propre jeu.

**Jeux convertis**  
– [Mr Seguin's Goat](https://auraes.itch.io/mr-seguins-goat)  
– [Parlez-lui d'amour](https://auraes.itch.io/parlez-lui-damour)  
– Celui qui voulait décrocher les étoiles  
– Land of the Giants  
(Le binaire se trouve dans le dossier des jeux concernés.)

**Crédits**  
The Quill is copyright Gilsoft and its creators 1983-2023.  
UnQuill is copyright 1996-2002, 2011, 2012, 2015, 2022 by John Elliott.  
Inform 6 Library is copyright Graham Nelson 1993-2004, David Griffith 2012-2023.  

Mr Seguin's Goat is copyright Auraes 2022-2023.  
Parlez-lui d'amour is copyright Auraes 2022-2023.

Celui qui voulait décrocher les étoiles is copyright Benjamin Roux 2008.

Land of the Giants is copyright Dorothy Millard 1989.  
http://members.optusnet.com.au/dorothyirene1/adv.html

**Logiciels et documentation externes**  
https://www.ifwiki.org/The_Quill  
http://8-bit.info/the-gilsoft-adventure-systems/  
https://www.seasip.info/Unix/UnQuill/index.html  
http://8bitag.com/info/documents/Quill-AdventureWriter-Reference.pdf  
https://s3-eu-west-2.amazonaws.com/quill-adventure-guides/main.html  
https://www.ifarchive.org/indexes/if-archiveXprogrammingXquill.html  
https://www.filfre.net/2013/07/the-quill/  
http://www.atarimania.com/game-atari-400-800-xl-xe-adventurewriter_20331.html

**Références : magazines français**  
TILT numéro 28, janvier-février 1986  
– The Quill, The Illustrator, The Patch, p. 92 à 93  
– Adventure Writer, p. 94 à 95  

TILT numéro 22, juin 1985  
– Adventure Writer, p. 89 à 90  

ORDI-5 numéro 13, septembre 1985  
– Quill de Gilsoft, p. 46 à 48  

Jeux & Stratégie numéro 34, août-septembre 1985  
– L’énigme du triangle, p. 43  

L’Atarien numéro 8, juin 1985  
– Adventure Writer, p. 7, p. 46  
– L’énigme du triangle, p. 6, p. 46  

Science & Vie Micro numéro 12, décembre 1984  
– Adventure Writer (publicité), p. 227
