Constant VISITED        11;
Constant ETOILE_PROCHE  12;
Constant ETOILE_BRILLE  13;
Constant FROMAGE_TROUVE 14;
Constant FROMAGE_MANGE  15;
Constant ETOILE_INTERET 16;

Array StatusTable table
	12
	',_' ',_'
	at_ Prologue
	message_ 0
	version_
	anykey_
	goto_ FaceMontagne
	desc_

	13
	',_' ',_'
	at_ Sentier
	notzero_ VISITED
	set_ VISITED
	anykey_
	goto_ Sommet
	desc_

	13
	',_' ',_'
	at_ Sommet
	absent_ Etoile
	zero_ ETOILE_BRILLE
	message_ 1
	set_ ETOILE_BRILLE

	11
	',_' ',_'
	at_ FaceMontagne
	carried_ boule_de_neige
	message_ 2
	destroy_ boule_de_neige

	13
	',_' ',_'
	at_ Sommet
	present_ Etoile
	zero_ ETOILE_PROCHE
	message_ 3
	set_ ETOILE_PROCHE

	9
	',_' ',_'
	at_ FaceMontagne
	clear_ 12
	clear_ ETOILE_BRILLE

	6
	',_' ',_'
	at_ Epilogue
	end_
	0
;

Array EventTable table
	12
	'nord' ',_'
	at_ FaceMontagne
	zero_ VISITED
	set_ VISITED
	goto_ Sentier
	desc_

	8
	'nord' ',_'
	at_ FaceMontagne
	goto_ Sommet
	desc_

	12
	'monter' ',_'
	at_ FaceMontagne
	zero_ VISITED
	set_ VISITED
	goto_ Sentier
	desc_

	8
	'monter' ',_'
	at_ FaceMontagne
	goto_ Sommet
	desc_

	8
	'entrer' 'taverne'
	at_ PlaceVillage
	goto_ Taverne
	desc_

	8
	'entrer' 'église'
	at_ PlaceVillage
	goto_ Eglise
	desc_

	8
	'entrer' 'confessionnal'
	at_ Eglise
	message_ 4
	done_

	8
	'entrer' ',_'
	at_ PlaceVillage
	message_ 5
	done_

	8
	'sauter' ',_'
	at_ Sommet
	message_ 6
	done_

	6
	'aller' ',_'
	message_ 7
	done_

	8
	'examiner' 'montagne'
	at_ FaceMontagne
	message_ 8
	done_

	8
	'examiner' 'montagne'
	at_ Sommet
	message_ 8
	done_

	8
	'examiner' 'sentier'
	at_ FaceMontagne
	message_ 9
	done_

	8
	'examiner' 'village'
	at_ FaceMontagne
	message_ 10
	done_

	6
	'examiner' 'village'
	message_ 11
	done_

	8
	'examiner' 'étoile'
	present_ Etoile
	message_ 12
	done_

	8
	'examiner' 'étoile'
	at_ Sommet
	message_ 13
	done_

	8
	'examiner' 'neige'
	at_ Sommet
	message_ 14
	done_

	8
	'examiner' 'boule'
	carried_ boule_de_neige
	message_ 15
	done_

	8
	'examiner' 'prêtre'
	at_ Eglise
	message_ 16
	done_

	8
	'examiner' 'confessionnal'
	at_ Eglise
	message_ 17
	done_

	8
	'examiner' 'autel'
	at_ Eglise
	message_ 18
	done_

	8
	'examiner' 'vitraux'
	at_ Eglise
	message_ 19
	done_

	8
	'examiner' 'bancs'
	at_ Eglise
	message_ 20
	done_

	8
	'examiner' 'église'
	at_ PlaceVillage
	message_ 11
	done_

	8
	'examiner' 'taverne'
	at_ PlaceVillage
	message_ 11
	done_

	8
	'examiner' 'taverne'
	at_ Taverne
	message_ 21
	done_

	8
	'examiner' 'fromage'
	carried_ fromage
	message_ 22
	done_

	8
	'examiner' 'fatras'
	at_ Taverne
	message_ 23
	done_

	6
	'examiner' ',_'
	message_ 24
	done_

	8
	'fouiller' 'taverne'
	at_ Taverne
	message_ 25
	done_

	16
	'fouiller' 'fatras'
	at_ Taverne
	zero_ FROMAGE_TROUVE
	message_ 26
	create_ fromage
	get_ fromage
	set_ FROMAGE_TROUVE
	done_

	6
	'fouiller' ',_'
	message_ 24
	done_

	12
	'prendre' 'étoile'
	present_ Etoile
	cls_
	message_ 27
	anykey_
	destroy_ Etoile
	desc_

	8
	'prendre' 'étoile'
	at_ Sommet
	message_ 6
	done_

	8
	'prendre' 'neige'
	carried_ boule_de_neige
	message_ 28
	done_

	14
	'prendre' 'neige'
	at_ Sommet
	notcarr_ boule_de_neige
	message_ 29
	create_ boule_de_neige
	get_ boule_de_neige
	done_

	12
	'prendre' 'fromage'
	at_ Taverne
	notcarr_ fromage
	zero_ FROMAGE_TROUVE
	message_ 30
	done_

	12
	'prendre' 'fromage'
	present_ fromage
	notcarr_ fromage
	get_ fromage
	message_ 31
	done_

	8
	'prendre' 'fatras'
	at_ Taverne
	message_ 32
	done_

	5
	'prendre' ',_'
	autog_
	ok_

	10
	'donner' 'fromage'
	at_ Sommet
	carried_ fromage
	message_ 33
	done_

	10
	'donner' 'fromage'
	at_ Eglise
	carried_ fromage
	message_ 34
	done_

	8
	'poser' 'boule'
	carried_ boule_de_neige
	message_ 35
	done_

	14
	'poser' 'fromage'
	at_ Taverne
	carried_ fromage
	message_ 36
	destroy_ fromage
	clear_ FROMAGE_TROUVE
	done_

	10
	'poser' 'fromage'
	carried_ fromage
	drop_ fromage
	message_ 37
	done_

	5
	'poser' ',_'
	autod_
	ok_

	18
	'lancer' 'boule'
	carried_ boule_de_neige
	zero_ ETOILE_INTERET
	set_ ETOILE_INTERET
	destroy_ boule_de_neige
	cls_
	message_ 38
	message_ 39
	anykey_
	desc_

	10
	'lancer' 'boule'
	carried_ boule_de_neige
	message_ 38
	destroy_ boule_de_neige
	done_

	14
	'lancer' 'fromage'
	at_ Taverne
	carried_ fromage
	message_ 36
	destroy_ fromage
	clear_ FROMAGE_TROUVE
	done_

	10
	'lancer' 'fromage'
	carried_ fromage
	drop_ fromage
	message_ 37
	done_

	5
	'lancer' ',_'
	autod_
	ok_

	10
	'manger' 'boule'
	carried_ boule_de_neige
	message_ 40
	destroy_ boule_de_neige
	done_

	12
	'manger' 'fromage'
	carried_ fromage
	message_ 41
	destroy_ fromage
	set_ FROMAGE_MANGE
	done_

	5
	'mettre' ',_'
	autow_
	ok_

	5
	'enlever' ',_'
	autor_
	ok_

	8
	'ouvrir' 'confessionnal'
	at_ Eglise
	message_ 4
	done_

	16
	'montrer' 'fromage'
	at_ Sommet
	carried_ fromage
	notzero_ ETOILE_INTERET
	cls_
	message_ 42
	anykey_
	goto_ Epilogue
	desc_

	10
	'montrer' 'fromage'
	at_ Sommet
	carried_ fromage
	message_ 43
	done_

	10
	'montrer' 'fromage'
	at_ Eglise
	carried_ fromage
	message_ 44
	done_

	6
	'nettoyer' ',_'
	message_ 45
	done_

	8
	'parler' 'prêtre'
	at_ Eglise
	message_ 46
	done_

	14
	'parler' 'fromage'
	at_ Eglise
	notzero_ FROMAGE_MANGE
	message_ 47
	clear_ FROMAGE_TROUVE
	clear_ FROMAGE_MANGE
	done_

	10
	'parler' 'fromage'
	at_ Eglise
	notcarr_ fromage
	message_ 48
	done_

	8
	'parler' 'fromage'
	at_ Eglise
	message_ 44
	done_

	8
	'parler' 'étoile'
	at_ Eglise
	message_ 49
	done_

	8
	'parler' 'étoile'
	at_ Sommet
	message_ 50
	done_

	8
	'parler' 'montagne'
	at_ Eglise
	message_ 51
	done_

	8
	'parler' 'village'
	at_ Eglise
	message_ 52
	done_

	8
	'parler' 'taverne'
	at_ Eglise
	message_ 52
	done_

	8
	'parler' 'église'
	at_ Eglise
	message_ 52
	done_

	8
	'parler' 'confessionnal'
	at_ Eglise
	message_ 53
	done_

	8
	'parler' ',_'
	at_ Eglise
	message_ 34
	done_

	6
	'parler' ',_'
	message_ 54
	done_

	8
	'attaquer' 'prêtre'
	at_ Eglise
	message_ 55
	done_

	8
	'embrasser' 'prêtre'
	at_ Eglise
	message_ 55
	done_

	8
	'prier' ',_'
	at_ Eglise
	message_ 56
	done_

	6
	'prier' ',_'
	message_ 57
	done_

	8
	'écouter' ',_'
	at_ Sommet
	message_ 58
	done_

	8
	'écouter' ',_'
	at_ PlaceVillage
	message_ 59
	done_

	6
	'écouter' ',_'
	message_ 59
	done_

	8
	'sentir' 'fromage'
	carried_ fromage
	message_ 60
	done_

	8
	'sentir' ',_'
	at_ Eglise
	message_ 61
	done_

	8
	'sentir' ',_'
	at_ Taverne
	message_ 62
	done_

	6
	'sentir' ',_'
	message_ 63
	done_

	6
	'attendre' ',_'
	message_ 64
	done_

	6
	'utiliser' ',_'
	message_ 65
	done_

	8
	'aide' ',_'
	cls_
	message_ 66
	anykey_
	desc_

	4
	'inventaire' ',_'
	inven_

	6
	'regarder' ',_'
	clear_ ETOILE_BRILLE
	desc_

	6
	'score' ',_'
	message_ 67
	done_

	6
	'quitter' ',_'
	quit_
	turns_
	end_

	4
	'sauver' ',_'
	save_

	4
	'charger' ',_'
	load_

	4
	'script' ',_'
	script_

	4
	'version' ',_'
	version_
	0
;

