Constant VISITED        11;
Constant ETOILE_PROCHE  12;
Constant ETOILE_BRILLE  13;
Constant FROMAGE_TROUVE 14;
Constant FROMAGE_MANGE  15;
Constant ETOILE_INTERET 16;

Array StatusTable table
#
* *
at Prologue
"^Celui qui voulait décrocher les étoiles^Un jeu d'aventure textuel.^(c) Yoruk, 2008. Modifié et adapté par Auraes.^Tapez AIDE pour plus d'informations.^"
version
anykey
goto FaceMontagne
desc

#
* *
at Sentier
notzero VISITED
set VISITED
anykey
goto Sommet
desc

#
* *
at Sommet
absent Etoile
zero ETOILE_BRILLE
"^Une petite étoile brille au-dessus de vous."
set ETOILE_BRILLE

#
* *
at FaceMontagne
carried boule_de_neige
"^La boule de neige se liquéfie au creux de votre main."
destroy boule_de_neige

#
* *
at Sommet
present Etoile
zero ETOILE_PROCHE
"^Est-elle morte ou vivante ?"
set ETOILE_PROCHE

#
* *
at FaceMontagne
clear 12
clear ETOILE_BRILLE

#
* *
at Epilogue
end
;

Array EventTable table
#
nord *
at FaceMontagne
zero VISITED
set VISITED
goto Sentier
desc

#
nord *
at FaceMontagne
goto Sommet
desc

#
monter *
at FaceMontagne
zero VISITED
set VISITED
goto Sentier
desc

#
monter *
at FaceMontagne
goto Sommet
desc

#
entrer taverne
at PlaceVillage
goto Taverne
desc

#
entrer église
at PlaceVillage
goto Eglise
desc

#
entrer confessionnal
at Eglise
"^Prêtre :^Auriez-vous quelques péchés véniels à expier ?"
done

#
entrer *
at PlaceVillage
"La taverne est au nord et l'église au sud."
done

#
sauter *
at Sommet
"L'étoile est hors d'atteinte."
done

#
aller *
"Utilisez de préférence les points cardinaux ou les verbes de déplacement."
done

#
examiner montagne
at FaceMontagne
"Cette montagne est l'une des plus hautes du monde. Une légende prétend que l'on peut toucher les étoiles lorsqu'on est au sommet.^^La neige fraîche crisse sous vos pieds."
done

#
examiner montagne
at Sommet
"Cette montagne est l'une des plus hautes du monde. Une légende prétend que l'on peut toucher les étoiles lorsqu'on est au sommet.^^La neige fraîche crisse sous vos pieds."
done

#
examiner sentier
at FaceMontagne
"Un sentier de terre battue, qui continue à l'est. Il vous mènera au village."
done

#
examiner village
at FaceMontagne
"Le village est à l'est."
done

#
examiner village
"Tous ces lieux semblent abandonnés... bizarre. Un village fantôme ?"
done

#
examiner étoile
present Etoile
"Une petite étoile. Elle vous suffira, si toutefois vous parvenez à la prendre."
done

#
examiner étoile
at Sommet
"Elle est si proche et pourtant inaccessible."
done

#
examiner neige
at Sommet
"De la neige probablement éternelle."
done

#
examiner boule
carried boule_de_neige
"Une belle boule de neige compacte et légère. Vous devriez essayer de la LANCER."
done

#
examiner prêtre
at Eglise
"Il semble le dernier être vivant de cet étrange village."
done

#
examiner confessionnal
at Eglise
"Un meuble austère et froid, adossé au mur de l'église."
done

#
examiner autel
at Eglise
"Autel au chœur de pierre, d'un village sacrificiel."
done

#
examiner vitraux
at Eglise
"Brûlés par la lumière divine et détruits par la caillasse."
done

#
examiner bancs
at Eglise
"Délaissée par les fidèles, l'église se reconvertit en garde-meubles."
done

#
examiner église
at PlaceVillage
"Tous ces lieux semblent abandonnés... bizarre. Un village fantôme ?"
done

#
examiner taverne
at PlaceVillage
"Tous ces lieux semblent abandonnés... bizarre. Un village fantôme ?"
done

#
examiner taverne
at Taverne
"Mais que s'est-il donc passé ici ?"
done

#
examiner fromage
carried fromage
"Un fromage d'alpage, où pâturent les troupeaux, là-haut sous les étoiles."
done

#
examiner fatras
at Taverne
"Tout est sens dessus dessous !"
done

#
examiner *
"Vous ne voyez rien de particulier."
done

#
fouiller taverne
at Taverne
"Bonne idée, mais par où commencer ?"
done

#
fouiller fatras
at Taverne
zero FROMAGE_TROUVE
"Vous trouver un vieux morceau de fromage. Vous le prenez avec vous."
create fromage
get fromage
set FROMAGE_TROUVE
done

#
fouiller *
"Vous ne voyez rien de particulier."
done

#
prendre étoile
present Etoile
cls
"Facile, pensez-vous. Vous tendez les bras en l'air.^Un petit saut... vous parvenez à peine à l'effleurer. Celle-ci, probablement craintive, s'est élevée de quelques centimètres."
anykey
destroy Etoile
desc

#
prendre étoile
at Sommet
"L'étoile est hors d'atteinte."
done

#
prendre neige
carried boule_de_neige
"Vous en avez assez d'une."
done

#
prendre neige
at Sommet
notcarr boule_de_neige
"Vous en faites une belle boule de neige, compacte et légère."
create boule_de_neige
get boule_de_neige
done

#
prendre fromage
at Taverne
notcarr fromage
zero FROMAGE_TROUVE
"Comment le trouver parmi tout ce fatras ?"
done

#
prendre fromage
present fromage
notcarr fromage
get fromage
"Vous récupérez votre précieux morceau de fromage."
done

#
prendre fatras
at Taverne
"Vous devriez essayer de le FOUILLER."
done

#
prendre *
autog
ok

#
donner fromage
at Sommet
carried fromage
"L'étoile est hors d'atteinte. Essayez plutôt de le lui MONTRER."
done

#
donner fromage
at Eglise
carried fromage
"Le prêtre lève les yeux vers vous. Il vous fixe sans ciller, mais ne répond rien."
done

#
poser boule
carried boule_de_neige
"Il serait dommage de ne pas la LANCER."
done

#
poser fromage
at Taverne
carried fromage
"Parmi tous ce désordre, cela va être difficile de le retrouver !"
destroy fromage
clear FROMAGE_TROUVE
done

#
poser fromage
carried fromage
drop fromage
"Il ne vous appartient plus."
done

#
poser *
autod
ok

#
lancer boule
carried boule_de_neige
zero ETOILE_INTERET
set ETOILE_INTERET
destroy boule_de_neige
cls
"Vous faites un pas glissé et projetez la boule au-delà de l'horizon."
"^L'étoile, jusqu'alors distraite, semble subitement s'intéresser à vous."
anykey
desc

#
lancer boule
carried boule_de_neige
"Vous faites un pas glissé et projetez la boule au-delà de l'horizon."
destroy boule_de_neige
done

#
lancer fromage
at Taverne
carried fromage
"Parmi tous ce désordre, cela va être difficile de le retrouver !"
destroy fromage
clear FROMAGE_TROUVE
done

#
lancer fromage
carried fromage
drop fromage
"Il ne vous appartient plus."
done

#
lancer *
autod
ok

#
manger boule
carried boule_de_neige
"Ce dessert glacé est rafraîchissant, mais sans grande saveur."
destroy boule_de_neige
done

#
manger fromage
carried fromage
"Vous le mangez avec d'autant plus de voracité qu'il ne vous était pas destiné."
destroy fromage
set FROMAGE_MANGE
done

#
mettre *
autow
ok

#
enlever *
autor
ok

#
ouvrir confessionnal
at Eglise
"^Prêtre :^Auriez-vous quelques péchés véniels à expier ?"
done

#
montrer fromage
at Sommet
carried fromage
notzero ETOILE_INTERET
cls
"Vous montrez le fromage à l'étoile. Intéressée, elle s'approche de vous en tourbillonnant, hésite, se méfie... et d'un ultime mouvement, fluide et gracieux, elle s'empare du fromage."
anykey
goto Epilogue
desc

#
montrer fromage
at Sommet
carried fromage
"Vous tendez le fromage vers le ciel, mais l'étoile regarde ailleurs."
done

#
montrer fromage
at Eglise
carried fromage
"^Prêtre :^Les étoiles adorent le fromage. Pourquoi ? Je n'en sais rien. La lune n'est-elle pas en fromage ?"
done

#
nettoyer *
"Cela ne servirait pas à grand-chose."
done

#
parler prêtre
at Eglise
"Si vous voulez un renseignement, interrogez-le sur quelque chose de précis."
done

#
parler fromage
at Eglise
notzero FROMAGE_MANGE
"^Prêtre :^Tu as péché par gourmandise, n'est-ce pas ? Te voilà pardonné."
clear FROMAGE_TROUVE
clear FROMAGE_MANGE
done

#
parler fromage
at Eglise
notcarr fromage
"^Prêtre :^Si vous en cherchez, le meilleur endroit pour en trouver semble être la taverne."
done

#
parler fromage
at Eglise
"^Prêtre :^Les étoiles adorent le fromage. Pourquoi ? Je n'en sais rien. La lune n'est-elle pas en fromage ?"
done

#
parler étoile
at Eglise
"^Prêtre :^Vous désirez attraper une étoile ? Peu de gens le savent, mais les étoiles raffolent du fromage !"
done

#
parler étoile
at Sommet
"Elle semble émettre un léger scintillement."
done

#
parler montagne
at Eglise
"^Prêtre :^On ne vous a pas menti. La montagne permet effectivement d'atteindre les étoiles."
done

#
parler village
at Eglise
"^Prêtre :^Je ne peux rien vous dire. Si vous saviez... Il est parfois préférable de ne pas connaître la vérité."
done

#
parler taverne
at Eglise
"^Prêtre :^Je ne peux rien vous dire. Si vous saviez... Il est parfois préférable de ne pas connaître la vérité."
done

#
parler église
at Eglise
"^Prêtre :^Je ne peux rien vous dire. Si vous saviez... Il est parfois préférable de ne pas connaître la vérité."
done

#
parler confessionnal
at Eglise
"^Prêtre :^Il renferme, à lui seul, tous les maux de l'humanité."
done

#
parler *
at Eglise
"Le prêtre lève les yeux vers vous. Il vous fixe sans ciller, mais ne répond rien."
done

#
parler *
"^Bla-bla-bla"
done

#
attaquer prêtre
at Eglise
"Laissez donc ce pauvre prêtre tranquille !"
done

#
embrasser prêtre
at Eglise
"Laissez donc ce pauvre prêtre tranquille !"
done

#
prier *
at Eglise
"Vous décidez de prier pour votre famille. En espérant qu'ils vont bien."
done

#
prier *
"Votre prière semble sans effet."
done

#
écouter *
at Sommet
"Vous n'entendez que le murmure essoufflé de votre respiration."
done

#
écouter *
at PlaceVillage
"Tout, ici, impose le silence."
done

#
écouter *
"Tout, ici, impose le silence."
done

#
sentir fromage
carried fromage
"L'odeur forte qu'il exhale vous met l'eau à la bouche."
done

#
sentir *
at Eglise
"Une odeur de vieille église, d'encens protecteur et purificateur."
done

#
sentir *
at Taverne
"Une odeur de renfermé, de poussière chaude et de houblon pourri."
done

#
sentir *
"Vous ne remarquez rien de particulier."
done

#
attendre *
"Un peu plus tard."
done

#
utiliser *
"Utilisez un verbe moins générique."
done

#
aide *
cls
"Saisissez vos commandes, en un ou deux mots, pour interagir et progresser dans le jeu (les articles et les prépositions sont ignorées). Le caractère * peut se substituer au second terme :^^>prendre pomme^>manger *^Délicieuse !^^Déplacez-vous selon les points cardinaux (n, s, e, o) ou par les verbes de déplacement : monter (m), descendre (d), entrer et sortir.^^Quelques verbes communs et utiles pour terminer le jeu :^Examiner (x), fouiller, prendre, poser, lancer, parler ;^Regarder (r, l) pour rafraîchir l'écran ;^Inventaire (i) pour afficher son contenu."
anykey
desc

#
inventaire *
inven

#
regarder *
clear ETOILE_BRILLE
desc

#
score *
"Il n'y a pas de score dans ce jeu."
done

#
quitter *
quit
turns
end

#
sauver *
save

#
charger *
load

#
script *
script

#
version *
version
;

