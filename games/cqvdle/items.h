Constant CAPACITY 4;
Constant LIGHT_OBJECT lumiere_vive;

!not_created
!at_worn
!at_carried

Object lumiere_vive
with
   describe "* Une source de lumière",
   at not_created,
has female;

Object fromage
with
   describe "* Un morceau de fromage",
   name 'fromage',
   at not_created,
has male;

Object boule_de_neige
with
   describe "* Une boule de neige",
   name 'boule',
   at not_created,
has female;

Object etoile
with
   describe "* Une étoile",
   name 'étoile',
   at Sommet,
has female;

