Array messages-->
!#0
	"^Celui qui voulait décrocher les étoiles^Un jeu d'aventure textuel.^(c) Yoruk, 2008. Modifié et adapté par Auraes.^Tapez AIDE pour plus d'informations.^"
!#1
	"^Une petite étoile brille au-dessus de vous."
!#2
	"^La boule de neige se liquéfie au creux de votre main."
!#3
	"^Est-elle morte ou vivante ?"
!#4
	"^Prêtre :^Auriez-vous quelques péchés véniels à expier ?"
!#5
	"La taverne est au nord et l'église au sud."
!#6
	"L'étoile est hors d'atteinte."
!#7
	"Utilisez de préférence les points cardinaux ou les verbes de déplacement."
!#8
	"Cette montagne est l'une des plus hautes du monde. Une légende prétend que l'on peut toucher les étoiles lorsqu'on est au sommet.^^La neige fraîche crisse sous vos pieds."
!#9
	"Un sentier de terre battue, qui continue à l'est. Il vous mènera au village."
!#10
	"Le village est à l'est."
!#11
	"Tous ces lieux semblent abandonnés... bizarre. Un village fantôme ?"
!#12
	"Une petite étoile. Elle vous suffira, si toutefois vous parvenez à la prendre."
!#13
	"Elle est si proche et pourtant inaccessible."
!#14
	"De la neige probablement éternelle."
!#15
	"Une belle boule de neige compacte et légère. Vous devriez essayer de la LANCER."
!#16
	"Il semble le dernier être vivant de cet étrange village."
!#17
	"Un meuble austère et froid, adossé au mur de l'église."
!#18
	"Autel au chœur de pierre, d'un village sacrificiel."
!#19
	"Brûlés par la lumière divine et détruits par la caillasse."
!#20
	"Délaissée par les fidèles, l'église se reconvertit en garde-meubles."
!#21
	"Mais que s'est-il donc passé ici ?"
!#22
	"Un fromage d'alpage, où pâturent les troupeaux, là-haut sous les étoiles."
!#23
	"Tout est sens dessus dessous !"
!#24
	"Vous ne voyez rien de particulier."
!#25
	"Bonne idée, mais par où commencer ?"
!#26
	"Vous trouver un vieux morceau de fromage. Vous le prenez avec vous."
!#27
	"Facile, pensez-vous. Vous tendez les bras en l'air.^Un petit saut... vous parvenez à peine à l'effleurer. Celle-ci, probablement craintive, s'est élevée de quelques centimètres."
!#28
	"Vous en avez assez d'une."
!#29
	"Vous en faites une belle boule de neige, compacte et légère."
!#30
	"Comment le trouver parmi tout ce fatras ?"
!#31
	"Vous récupérez votre précieux morceau de fromage."
!#32
	"Vous devriez essayer de le FOUILLER."
!#33
	"L'étoile est hors d'atteinte. Essayez plutôt de le lui MONTRER."
!#34
	"Le prêtre lève les yeux vers vous. Il vous fixe sans ciller, mais ne répond rien."
!#35
	"Il serait dommage de ne pas la LANCER."
!#36
	"Parmi tous ce désordre, cela va être difficile de le retrouver !"
!#37
	"Il ne vous appartient plus."
!#38
	"Vous faites un pas glissé et projetez la boule au-delà de l'horizon."
!#39
	"^L'étoile, jusqu'alors distraite, semble subitement s'intéresser à vous."
!#40
	"Ce dessert glacé est rafraîchissant, mais sans grande saveur."
!#41
	"Vous le mangez avec d'autant plus de voracité qu'il ne vous était pas destiné."
!#42
	"Vous montrez le fromage à l'étoile. Intéressée, elle s'approche de vous en tourbillonnant, hésite, se méfie... et d'un ultime mouvement, fluide et gracieux, elle s'empare du fromage."
!#43
	"Vous tendez le fromage vers le ciel, mais l'étoile regarde ailleurs."
!#44
	"^Prêtre :^Les étoiles adorent le fromage. Pourquoi ? Je n'en sais rien. La lune n'est-elle pas en fromage ?"
!#45
	"Cela ne servirait pas à grand-chose."
!#46
	"Si vous voulez un renseignement, interrogez-le sur quelque chose de précis."
!#47
	"^Prêtre :^Tu as péché par gourmandise, n'est-ce pas ? Te voilà pardonné."
!#48
	"^Prêtre :^Si vous en cherchez, le meilleur endroit pour en trouver semble être la taverne."
!#49
	"^Prêtre :^Vous désirez attraper une étoile ? Peu de gens le savent, mais les étoiles raffolent du fromage !"
!#50
	"Elle semble émettre un léger scintillement."
!#51
	"^Prêtre :^On ne vous a pas menti. La montagne permet effectivement d'atteindre les étoiles."
!#52
	"^Prêtre :^Je ne peux rien vous dire. Si vous saviez... Il est parfois préférable de ne pas connaître la vérité."
!#53
	"^Prêtre :^Il renferme, à lui seul, tous les maux de l'humanité."
!#54
	"^Bla-bla-bla"
!#55
	"Laissez donc ce pauvre prêtre tranquille !"
!#56
	"Vous décidez de prier pour votre famille. En espérant qu'ils vont bien."
!#57
	"Votre prière semble sans effet."
!#58
	"Vous n'entendez que le murmure essoufflé de votre respiration."
!#59
	"Tout, ici, impose le silence."
!#60
	"L'odeur forte qu'il exhale vous met l'eau à la bouche."
!#61
	"Une odeur de vieille église, d'encens protecteur et purificateur."
!#62
	"Une odeur de renfermé, de poussière chaude et de houblon pourri."
!#63
	"Vous ne remarquez rien de particulier."
!#64
	"Un peu plus tard."
!#65
	"Utilisez un verbe moins générique."
!#66
	"Saisissez vos commandes, en un ou deux mots, pour interagir et progresser dans le jeu (les articles et les prépositions sont ignorées). Le caractère * peut se substituer au second terme :^^>prendre pomme^>manger *^Délicieuse !^^Déplacez-vous selon les points cardinaux (n, s, e, o) ou par les verbes de déplacement : monter (m), descendre (d), entrer et sortir.^^Quelques verbes communs et utiles pour terminer le jeu :^Examiner (x), fouiller, prendre, poser, lancer, parler ;^Regarder (r, l) pour rafraîchir l'écran ;^Inventaire (i) pour afficher son contenu."
!#67
	"Il n'y a pas de score dans ce jeu."
; !End_Array
