Constant START_ROOM Prologue;

Object Prologue
with
   describe "Après avoir marché plusieurs jours dans des contrées lointaines, \
   vous êtes enfin arrivé au pied de la montagne. La Montagne, celle qui est, \
   soi-disant, tellement haute que l'on peut toucher les étoiles lorsqu'on \
   est au sommet.";

Object FaceMontagne
with
   describe "Une immense montagne vous fait face. Vous pouvez grimper à \
   son sommet en continuant au nord, ou bien aller au village. Pour cela, il \
   vous faut poursuivre le petit sentier qui vous a guidé jusqu'ici, \
   c'est-à-dire en continuant à l'est.",
   exits 'est' PlaceVillage;

Object Sentier
with
   describe "Vous commencez à gravir la montagne...^\
   Après plusieurs heures de marche, vous atteignez enfin le sommet.";

Object Sommet
with
   describe "Le sommet de la montagne est en grande partie enneigé. \
   Vous pouvez redescendre par le sud.",
   exits 'sud' FaceMontagne 'descendre' FaceMontagne;

Object PlaceVillage
with
   describe "Vous êtes sur la place principale d'un petit village, situé à \
   l'est de la montagne. Divers bâtiments l'entourent : une taverne au nord \
   et une église au sud.",
   exits 'nord' Taverne 'sud' Eglise 'ouest' FaceMontagne;

Object Taverne
with
   describe "La taverne semble abandonnée depuis très longtemps. Tout ici a \
   été bousculé, comme si les derniers occupants étaient partis \
   précipitamment.^^Un fatras indescriptible encombre le lieu.",
   exits 'sud' PlaceVillage 'sortir' PlaceVillage;

Object Eglise
with
   describe "L'église ressemble à tous les autres lieux de cet étrange \
   village abandonné. Quelques bancs sont entassés derrière l'autel, et les \
   vitraux ont été brisés.^^Un prêtre se tient assis près du confessionnal.",
   exits 'nord' PlaceVillage 'sortir' PlaceVillage;

Object Epilogue
with
   describe "L'étoile commence à le grignoter... Vous en profitez pour \
   l'attraper ! Cela ne semble pas la déranger.^Ça y est ! Vous avez réussi à \
   décrocher une étoile. Après avoir voyagé pendant tant de semaines, \
   vous êtes enfin parvenu à réaliser votre rêve !";
