Constant FLAG_KEY 20;
Constant FLAG_BEAN 21;

Array StatusTable table
	9
	'd//' ',_'
	at_ intro0
	anykey_
	goto_ intro2
	desc_

	9
	'd//' ',_'
	at_ intro1
	anykey_
	goto_ cottage
	desc_

	9
	'd//' ',_'
	at_ intro2
	anykey_
	goto_ intro1
	desc_
	0
;

Array EventTable table
	10
	'n//' ',_'
	at_ gallery
	absent_ open_door
	message_ 0
	done_

	10
	'n//' ',_'
	at_ gallery
	present_ open_door
	goto_ huge_room
	desc_

	10
	'w//' ',_'
	at_ huge_room
	absent_ broken_door
	message_ 0
	done_

	10
	'w//' ',_'
	at_ huge_room
	present_ broken_door
	goto_ green_room
	desc_

	8
	'x//' 'bean'
	present_ bean
	message_ 1
	done_

	8
	'x//' 'bean'
	present_ beanstalk
	message_ 2
	done_

	14
	'x//' 'bench'
	at_ kitchen
	zero_ FLAG_KEY
	message_ 3
	create_ key
	set_ FLAG_KEY
	done_

	10
	'x//' 'bench'
	at_ kitchen
	notzero_ FLAG_KEY
	message_ 4
	done_

	8
	'x//' 'key'
	present_ key
	message_ 5
	done_

	8
	'x//' 'mouse'
	present_ mouse
	message_ 6
	done_

	8
	'x//' 'egg'
	present_ egg
	message_ 7
	done_

	10
	'x//' 'garden'
	at_ garden
	absent_ beanstalk
	message_ 8
	done_

	10
	'x//' 'garden'
	at_ garden
	present_ beanstalk
	message_ 9
	done_

	8
	'x//' 'shed'
	at_ garden
	message_ 10
	done_

	8
	'x//' 'mother'
	present_ mother_1
	message_ 11
	done_

	8
	'x//' 'mother'
	present_ mother_2
	message_ 11
	done_

	8
	'x//' 'mother'
	present_ mother_3
	message_ 12
	done_

	10
	'x//' 'children'
	present_ children
	notat_ cottage
	message_ 13
	done_

	8
	'x//' 'axe'
	present_ axe
	message_ 14
	done_

	8
	'x//' 'giant'
	present_ giant
	message_ 15
	done_

	8
	'x//' 'gallery'
	at_ gallery
	message_ 16
	done_

	8
	'x//' 'kitchen'
	at_ kitchen
	message_ 17
	done_

	8
	'x//' 'castle'
	atlt_ top_of_beanstalk
	message_ 18
	done_

	6
	'x//' 'castle'
	message_ 19
	done_

	8
	'x//' 'cottage'
	at_ cottage
	message_ 20
	done_

	6
	'x//' ',_'
	message_ 4
	done_

	10
	'plant' 'bean'
	notcarr_ bean
	zero_ FLAG_BEAN
	message_ 21
	done_

	16
	'plant' 'bean'
	at_ garden
	carried_ bean
	destroy_ bean
	create_ beanstalk
	set_ FLAG_BEAN
	message_ 22
	done_

	10
	'plant' 'bean'
	notat_ garden
	zero_ FLAG_BEAN
	message_ 23
	done_

	6
	'plant' ',_'
	message_ 24
	done_

	14
	'open' 'door'
	at_ gallery
	absent_ open_door
	carried_ key
	message_ 25
	create_ open_door
	done_

	12
	'open' 'door'
	at_ huge_room
	absent_ broken_door
	carried_ key
	message_ 26
	done_

	8
	'open' 'door'
	at_ gallery
	message_ 27
	done_

	10
	'open' 'door'
	at_ huge_room
	notcarr_ key
	message_ 0
	done_

	6
	'open' ',_'
	message_ 24
	done_

	14
	'break' 'door'
	at_ huge_room
	absent_ broken_door
	carried_ axe
	message_ 28
	create_ broken_door
	done_

	12
	'break' 'door'
	at_ huge_room
	absent_ broken_door
	notcarr_ axe
	message_ 29
	done_

	10
	'break' 'door'
	at_ huge_room
	present_ broken_door
	message_ 30
	done_

	8
	'break' 'bean'
	present_ beanstalk
	message_ 31
	done_

	6
	'break' ',_'
	message_ 24
	done_

	10
	'u//' 'bean'
	at_ garden
	present_ beanstalk
	goto_ top_of_beanstalk
	desc_

	10
	'go' 'door'
	at_ gallery
	absent_ open_door
	message_ 0
	done_

	10
	'go' 'door'
	at_ gallery
	present_ open_door
	goto_ huge_room
	desc_

	10
	'go' 'door'
	at_ huge_room
	present_ broken_door
	goto_ green_room
	desc_

	6
	'help' ',_'
	message_ 32
	done_

	17
	'give' 'egg'
	at_ cottage
	present_ mother_1
	carried_ egg
	swap_ mother_1 mother_2
	destroy_ egg
	message_ 33
	done_

	15
	'give' 'egg'
	at_ cottage
	present_ mother_3
	carried_ egg
	cls_
	message_ 34
	message_ 35
	end_

	17
	'give' 'children'
	at_ cottage
	present_ mother_1
	carried_ children
	swap_ mother_1 mother_3
	destroy_ children
	message_ 36
	done_

	15
	'give' 'children'
	at_ cottage
	present_ mother_2
	carried_ children
	cls_
	message_ 37
	message_ 35
	end_

	8
	'hit' 'giant'
	present_ giant
	message_ 38
	done_

	6
	'wait' ',_'
	message_ 39
	done_

	6
	'turn' ',_'
	message_ 40
	done_

	6
	'twist' ',_'
	message_ 41
	done_

	6
	'pull' ',_'
	message_ 42
	done_

	6
	'push' ',_'
	message_ 43
	done_

	6
	'get' 'bean'
	get_ bean
	ok_

	6
	'get' 'key'
	get_ key
	ok_

	6
	'get' 'mouse'
	get_ mouse
	ok_

	8
	'get' 'egg'
	notat_ huge_room
	get_ egg
	ok_

	10
	'get' 'egg'
	at_ huge_room
	present_ giant
	message_ 44
	done_

	10
	'get' 'egg'
	at_ huge_room
	absent_ giant
	get_ egg
	ok_

	6
	'get' 'children'
	get_ children
	ok_

	6
	'get' 'axe'
	get_ axe
	ok_

	4
	'get' 'i//'
	inven_

	6
	'drop' 'bean'
	drop_ bean
	ok_

	6
	'drop' 'key'
	drop_ key
	ok_

	8
	'drop' 'mouse'
	notat_ huge_room
	drop_ mouse
	ok_

	14
	'drop' 'mouse'
	at_ huge_room
	present_ giant
	message_ 45
	destroy_ giant
	destroy_ mouse
	done_

	6
	'drop' 'egg'
	drop_ egg
	ok_

	6
	'drop' 'children'
	drop_ children
	ok_

	6
	'drop' 'axe'
	drop_ axe
	ok_

	4
	'i//' ',_'
	inven_

	4
	'r//' ',_'
	desc_

	6
	'q//' ',_'
	quit_
	turns_
	end_

	4
	'save' ',_'
	save_

	4
	'load' ',_'
	load_

	6
	'verify' ',_'
	message_ 46
	done_
	0
;
