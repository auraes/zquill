Constant CAPACITY 3;
Constant LIGHT_OBJECT mouse;

!0
Object mouse
with
   describe "* A tiny mouse",
   at dining_room;
!1
Object key
with
   describe "* A silver key",
   at not_created;
!2
Object axe
with
   describe "* An axe",
   at garden_shed;
!3
Object mother_1
with
   describe "* Mother is here",
   at cottage;
!4
Object children
with
   describe "* The Children, Peter and Mary, are here.",
   at green_room;
!5
Object giant
with
   describe "* The --- G I A N T ---",
   at huge_room;
!6
Object egg
with
   describe "* The GOLDEN EGG",
   at huge_room;
!7
Object bean
with
   describe "* A bean",
   at cottage;
!8
Object beanstalk
with
   describe "* A giant beanstalk",
   at not_created;
!9
Object open_door
with
   describe "* An open door",
   at not_created;
!10
Object broken_door
with
   describe "* A broken door",
   at not_created;
!11
Object mother_2
with
   describe "* Mother is here holding the GOLDEN EGG.",
   at not_created;
 !12 
Object mother_3
with
   describe "* Mother is here with THE CHILDREN.",
   at not_created;
