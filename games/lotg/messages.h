Array messages-->
!#0
	"The door is locked."
!#1
	"It's a magic bean, perhaps you should plant it."
!#2
	"Perhaps you could climb it!"
!#3
	"On the bench you find a silver key."
!#4
	"You see nothing special."
!#5
	"It's just a silver key."
!#6
	"It's just a tiny mouse."
!#7
	"It looks valuable - it could set you up for life!"
!#8
	"The garden looks ready for planting."
!#9
	"The garden has a giant beanstalk growing in it."
!#10
	"You see nothing special about the shed."
!#11
	"Mother is unhappy because Peter and Mary have been captured by the mean Giant."
!#12
	"Mother is happy now that the children are back home."
!#13
	"They look frightened."
!#14
	"Looks sharp!"
!#15
	"He looks mean!"
!#16
	"You see nothing special about the gallery."
!#17
	"You see nothing special about the kitchen."
!#18
	"This is where the mean giant lives!"
!#19
	"This is the castle where the giant lives."
!#20
	"You see nothing special about the cottage."
!#21
	"You don't have it."
!#22
	"You plant the bean which grows very fast into a giant beanstalk."
!#23
	"You can't do that here!"
!#24
	"You can't do that."
!#25
	"OK. You unlock the door with the silver key."
!#26
	"The key doesn't fit this lock."
!#27
	"It's already open."
!#28
	"You break down the door with the axe."
!#29
	"How?"
!#30
	"It's already broken."
!#31
	"That wouldn't be wise."
!#32
	"Sorry!"
!#33
	"Mother is pleased with the GOLDEN EGG, now if only you could find THE CHILDREN."
!#34
	"Mother is so pleased that you have been able to find the GOLDEN EGG which will set you up for life and with THE CHILDREN home safe and sound you all live happily ever after."
!#35
	"^CONGRATULATIONS^^This game is over^"
!#36
	"Mother is so pleased to see THE CHILDREN again, now if only you could find the GOLDEN EGG."
!#37
	"Mother is so pleased to see THE CHILDREN again and with the GOLDEN EGG you are set up for life and live happily ever after."
!#38
	"You must be joking!!! You wouldn't stand a chance!"
!#39
	"What for?"
!#40
	"You don't need to turn anything to complete this adventure."
!#41
	"You don't need to twist anything to complete this adventure."
!#42
	"You don't need to pull anything to complete this adventure."
!#43
	"You don't need to push anything to complete this adventure."
!#44
	"The giant won't let you."
!#45
	"The mouse frightens the giant who runs away as fast as he can."
!#46
	"Version 1.2"
; !End_Array
