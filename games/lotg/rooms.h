Constant START_ROOM intro0;

Object intro0
with
   describe "LAND of the GIANTS^^A children's adventure, dedicated to \
   Rachael Millard.^^Written by Dorothy Millard, (c) 1989.";

Object intro1
with
   describe "LAND OF THE GIANTS^^To move from place to place type North, \
   South, East, West or their abbreviations N, S, E, W. Try also Up and \
   Down.^^(I)nventory gives you a list of what you are carrying.^^(R)edefine \
   will describe your current location.^^(X) is an abbreviation for Examine.\
   ^^(Q)uit if you have had enough.^^VERBS - Examine, Plant, Open, Unlock, \
   Break, Go, Get, Take, Drop, Give.";

Object intro2
with
   describe "LAND OF THE GIANTS^^The Children, Peter and Mary, have been \
   captured by the mean GIANT. You have been chosen to rescue them.^^You start \
   in your cottage with Mother. Can you rescue the children and find \
   the GOLDEN EGG?";

Object cottage
with
   describe "You are in a your small quaint cottage. You can go EAST into the 
   garden.",
   exits 'e//' garden;

Object garden
with
   describe "You are in the garden. You can go WEST into the house and SOUTH 
   into the shed.",
   exits 's//' garden_shed 'w//' cottage;

Object garden_shed
with
   describe "You are in the garden shed. You can go NORTH into the garden.",
   exits 'n//' garden;

Object top_of_beanstalk
with
   describe "You are at the top of the beanstalk. You can see a large castle in 
   the distance. You can go EAST and DOWN the beanstalk.",
   exits 'd//' garden 'e//' castle_entrance;

Object castle_entrance
with
   describe "You are in the Castle entrance. You can go NORTH, WEST and SOUTH.",
   exits 'n//' dining_room 's//' kitchen 'w//' top_of_beanstalk;

Object kitchen
with
   describe "You are in the Castle Kitchen. You can go NORTH. There is a \
   bench here.",
   exits 'n//' castle_entrance;

Object dining_room
with
   describe "You are in the Castle Dining Room. You can go WEST and SOUTH.",
   exits 's//' castle_entrance 'w//' gallery;

Object gallery
with
   describe "You are in the Castle Gallery. You can go EAST and there is a door 
   to the NORTH.",
   exits 'e//' dining_room;
  
Object huge_room
with
   describe "You are in a Huge Room in the Castle. There are doors to the WEST 
   and SOUTH.",
   exits 's//' gallery;

Object green_room
with
   describe "You are in a Green Room in the Castle. You can go EAST.",
   exits 'e//' huge_room;

