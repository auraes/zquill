Constant INK_DEFAULT CLR_CURRENT;
Constant PAPER_DEFAULT CLR_CURRENT;
Constant PROMPT ">";
Constant YES 'y';
Constant CENTERING_SYSMESS_16 25;

Array sysmess-->
!0
   "Everything is dark. You can't see.^"      
!1
   "^You can also see:-^"
!2
   "^What next?^"
!3
   "^What next?^"                          
!4
   "^What next?^"
!5
   "^What next?^"
!6
   "Sorry, I don't understand you.^"
!7
   "You can't go that way.^"
!8
   "You can't.^"
!9
   "You have with you:-^"
!10
   " (worn)"
!11
   "Nothing at all.^"
!12
   "Do you really want to quit now?^"
!13
   "^END OF GAME^^Do you want to try again?^"
!14
   "Bye for now.^"
!15
   "OK.^"
!16
   "Press any key to continue"
!17
   "You have taken "
!18
   " turn"
!19
   "s"
!20
   ".^"
!21
   "You have scored " 
!22
   "%^"
!23
   "You're not wearing it.^"
!24
   "You can't carry any more.^"
!25
   "You already have it.^"
!26
   "It's not here.^"
!27
   "You can't carry any more.^"
!28
   "You don't have it.^"
!29
   "You're already wearing it.^"
!30
   "Y^"
!31
   "N^"
!32
   "Save failed.^"
!33
   "Restore failed.^"
!34
   "^"
; !End_Array

