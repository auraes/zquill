Constant FLAG_ESSAI 5;
Constant FLAG_CROA 6;
Constant FLAG_CORBEAU 11;
Constant FLAG_BOTTE 12;
Constant FLAG_BOUTEILLE 13;
Constant FLAG_MUR 14;
Constant FLAG_BUG 15;
Constant FLAG_NAGE 16;
Constant FLAG_NOTE 17;
Constant FLAG_POMME 18;
Constant FLAG_CLE 19;
Constant FLAG_TROGNON 20;
Constant FLAG_PASSAGE 21;
Constant FLAG_CARTE 22;
Constant FLAG_PAPILLON 23;
Constant FLAG_PERLE 24;
Constant FLAG_INTERLUDE 25;
Constant FLAG_BEBE 26;
Constant FLAG_RACCOURCI 27;
Constant FLAG_REVE 28;
Constant FLAG_SCORE 29;

Array StatusTable table
   28
   at_ Prologue
   anykey_
   picture_ 14 Bord_lac
   picture_ 20 Foret
   picture_ 34 Clairiere
   picture_ 11 Devant_grotte
   picture_ 18 Dans_grotte
   picture_ 39 Interieur_tour
   picture_ 6 Cachot
   goto_ Prologue_suite
   desc_

   8
   at_ Prologue_suite
   version_
   anykey_
   goto_ Bord_lac
   desc_

   7
   at_ Fake_room1
   anykey_
   goto_ foret
   desc_

   7
   at_ Fake_room2
   anykey_
   goto_ Interieur_tour
   desc_

   7
   at_ Fake_room3
   anykey_
   goto_ Clairiere
   desc_

   7
   at_ Fake_room4
   anykey_
   goto_ Obscurite_cachot
   desc_

   7
   at_ Fake_room5
   anykey_
   goto_ Cachot
   desc_

   10
   at_ Exterieur_tour
   carried_ o_perle
   place_ o_perle Foret
   message_ 116

   17
   at_ Exterieur_tour
   carried_ o_note
   destroy_ o_note
   set_ FLAG_BOUTEILLE
   place_ o_bouteille Foret
   picture_ 22 Foret
   message_ 53

   10
   at_ Exterieur_tour
   carried_ o_botte_vide
   place_ o_botte_vide Bord_lac
   message_ 101

   11
   at_ Exterieur_tour
   anykey_
   cls_
   message_ 57
   anykey_
   goto_ Interieur_tour
   desc_

   7
   at_ Sommet_tour
   notzero_ FLAG_CROA
   message_ 20

   9
   at_ Foret
   carried_ o_torche
   destroy_ o_torche
   message_ 130

   17
   at_ Dans_lac
   carried_ o_note
   destroy_ o_note
   set_ FLAG_BOUTEILLE
   place_ o_bouteille Foret
   picture_ 22 Foret
   message_ 53

   8
   at_ Dans_lac
   zero_ FLAG_BOUTEILLE
   picture_ 23 Foret

   16
   at_ Dans_lac
   set_ FLAG_POMME
   destroy_ o_pomme_arbre
   destroy_ o_botte
   place_ o_botte_vide Bord_lac
   anykey_
   goto_ Fond_lac
   desc_

   7
   at_ Fond_lac1
   anykey_
   goto_ Fond_lac2
   desc_

   7
   at_ Fond_lac2
   anykey_
   goto_ Bord_lac
   desc_

   6
   at_ Epilogue
   score_
   turns_
   end_

   0
;

Array EventTable table
   16
   'nord' ',_'
   at_ Bord_lac
   zero_ FLAG_NAGE
   ink_ CLR_MAGENTA
   message_ 43
   ink_ INK_DEFAULT
   message_ 39
   done_

   10
   'nord' ',_'
   at_ Bord_lac
   carried_ o_botte_pleine
   message_ 98
   done_

   10
   'nord' ',_'
   at_ Bord_lac
   present_ o_poisson
   message_ 93
   done_

   13
   'nord' ',_'
   at_ Bord_lac
   present_ o_tour
   swap_ o_tour o_tour_flamme
   goto_ Exterieur_tour
   desc_

   10
   'nord' ',_'
   at_ Bord_lac
   present_ o_tour_flamme
   cls_
   message_ 176

   14
   'nord' ',_'
   at_ Bord_lac
   present_ o_tour_flamme
   carried_ o_pelle
   place_ o_pelle Bord_lac
   message_ 159

   13
   'nord' ',_'
   at_ Bord_lac
   present_ o_tour_flamme
   carried_ o_poisson_mort
   destroy_ o_poisson_mort
   message_ 248

   13
   'nord' ',_'
   at_ Bord_lac
   present_ o_tour_flamme
   carried_ o_perle
   destroy_ o_perle
   message_ 116

   14
   'nord' ',_'
   at_ Bord_lac
   present_ o_tour_flamme
   carried_ o_cle
   place_ o_cle Clairiere
   message_ 212

   15
   'nord' ',_'
   at_ Bord_lac
   present_ o_tour_flamme
   destroy_ o_tour_flamme
   message_ 111
   anykey_
   goto_ Cachot
   desc_

   12
   'nord' ',_'
   at_ Bord_lac
   absent_ o_tour
   absent_ o_tour_flamme
   message_ 40
   done_

   10
   'nord' ',_'
   atgt_ Interieur_tour
   atlt_ Cachot
   message_ 102
   done_

   8
   'nord' ',_'
   at_ Devant_grotte
   message_ 83
   done_

   8
   'nord' ',_'
   at_ Dans_grotte
   message_ 83
   done_

   10
   'sud' ',_'
   atgt_ Interieur_tour
   atlt_ Cachot
   message_ 102
   done_

   10
   'sud' ',_'
   at_ Bord_lac
   notzero_ FLAG_RACCOURCI
   goto_ Clairiere
   desc_

   10
   'est' ',_'
   at_ Foret
   notzero_ FLAG_PAPILLON
   goto_ Dans_grotte
   desc_

   21
   'est' ',_'
   at_ Foret
   present_ o_papillon
   destroy_ o_papillon
   set_ FLAG_PAPILLON
   picture_ 23 Foret
   cls_
   message_ 181
   anykey_
   goto_ Dans_grotte
   desc_

   14
   'est' ',_'
   at_ Foret
   ink_ CLR_MAGENTA
   message_ 43
   ink_ INK_DEFAULT
   message_ 13
   done_

   10
   'est' ',_'
   atgt_ Interieur_tour
   atlt_ Cachot
   message_ 102
   done_

   10
   'est' ',_'
   at_ Dans_grotte
   notzero_ FLAG_PAPILLON
   goto_ Foret
   desc_

   12
   'est' ',_'
   at_ Dans_grotte
   carried_ o_pelle
   carried_ o_torche
   message_ 126
   done_

   16
   'est' ',_'
   at_ Dans_grotte
   carried_ o_torche
   present_ o_cle
   notcarr_ o_cle
   get_ o_cle
   message_ 129
   anykey_

   16
   'est' ',_'
   at_ Dans_grotte
   carried_ o_torche
   set_ FLAG_PASSAGE
   cls_
   goto_ Foret
   message_ 128
   anykey_
   desc_

   12
   'est' ',_'
   at_ Dans_grotte
   zero_ FLAG_PASSAGE
   cls_
   message_ 127
   anykey_
   desc_

   12
   'ouest' ',_'
   at_ Bord_lac
   zero_ FLAG_MUR
   set_ FLAG_MUR
   message_ 36
   done_

   10
   'ouest' ',_'
   atgt_ Interieur_tour
   atlt_ Cachot
   message_ 102
   done_

   8
   'ouest' ',_'
   at_ Cachot
   message_ 201
   done_

   15
   'ouest' ',_'
   at_ Clairiere
   zero_ FLAG_RACCOURCI
   set_ FLAG_RACCOURCI
   message_ 213
   anykey_
   goto_ Bord_lac
   desc_

   8
   'ouest' ',_'
   at_ Clairiere
   goto_ Bord_lac
   desc_

   11
   'monter' ',_'
   at_ Etage_trone
   let_ FLAG_CROA 1
   goto_ Sommet_tour
   desc_

   8
   'monter' ',_'
   at_ Obscurite_cachot
   message_ 105
   done_

   8
   'monter' ',_'
   at_ Etage_13
   message_ 164
   done_

   12
   'descendre' ',_'
   at_ Devant_grotte
   notzero_ FLAG_CORBEAU
   clear_ FLAG_CORBEAU
   goto_ Fake_room1
   desc_

   8
   'descendre' ',_'
   at_ Devant_grotte
   goto_ foret
   desc_

   10
   'descendre' ',_'
   at_ Sommet_arbre
   zero_ FLAG_POMME
   goto_ Dans_arbre_pomme
   desc_

   8
   'descendre' ',_'
   at_ Sommet_arbre
   goto_ Dans_arbre
   desc_

   10
   'descendre' ',_'
   at_ Interieur_tour
   notzero_ FLAG_TROGNON
   message_ 106
   done_

   8
   'descendre' ',_'
   at_ Interieur_tour
   goto_ Etage_1
   desc_

   16
   'descendre' ',_'
   at_ Etage_12
   create_ o_pomme
   get_ o_pomme
   cls_
   message_ 59
   anykey_
   goto_ Obscurite_cachot
   desc_

   8
   'descendre' ',_'
   at_ Obscurite_cachot
   message_ 134
   done_

   8
   'descendre' ',_'
   at_ Etage_13
   message_ 165
   done_

   8
   'entrer' ',_'
   at_ Devant_grotte
   message_ 83
   done_

   8
   'entrer' ',_'
   at_ Clairiere
   message_ 65
   done_

   8
   'sortir' ',_'
   at_ Dans_grotte
   message_ 83
   done_

   8
   'sortir' ',_'
   at_ Interieur_tour
   message_ 135
   done_

   8
   'sortir' ',_'
   at_ Etage_13
   message_ 166
   done_

   10
   'sortir' ',_'
   atgt_ Dans_lac
   atlt_ Fond_lac1
   message_ 134
   done_

   8
   'sortir' ',_'
   at_ Cachot
   message_ 201
   done_

   8
   'sortir' ',_'
   at_ Obscurite_cachot
   message_ 166
   done_

   8
   'sauter' ',_'
   at_ Dans_arbre
   goto_ Foret
   desc_

   8
   'sauter' ',_'
   at_ Dans_arbre_pomme
   goto_ Foret
   desc_

   8
   'sauter' 'souche'
   at_ Clairiere
   message_ 64
   done_

   14
   'sauter' ',_'
   at_ Sommet_arbre
   ink_ CLR_MAGENTA
   message_ 43
   ink_ INK_DEFAULT
   message_ 54
   done_

   13
   'sauter' ',_'
   at_ Foret
   worn_ o_botte
   message_ 14
   anykey_
   goto_ Dans_arbre_pomme
   desc_

   8
   'sauter' ',_'
   worn_ o_botte
   message_ 15
   done_

   6
   'sauter' ',_'
   message_ 55
   done_

   6
   'aller' ',_'
   message_ 2
   done_

   8
   'grimper' 'arbre'
   at_ Foret
   notzero_ FLAG_POMME
   dropall_

   11
   'grimper' 'arbre'
   at_ Foret
   notzero_ FLAG_POMME
   present_ o_note
   get_ o_note

   10
   'grimper' 'arbre'
   at_ Foret
   notzero_ FLAG_POMME
   goto_ Dans_arbre
   desc_

   8
   'grimper' 'arbre'
   at_ Foret
   message_ 16
   done_

   8
   'grimper' 'escalier'
   at_ Foret
   goto_ Devant_grotte
   desc_

   8
   'grimper' 'mur'
   at_ Bord_lac
   message_ 38
   done_

   8
   'grimper' 'souche'
   at_ Clairiere
   message_ 64
   done_

   8
   'grimper' 'gargouille'
   at_ Sommet_tour
   message_ 157
   done_

   8
   'nager' ',_'
   at_ Bord_lac
   message_ 40
   done_

   8
   'nager' ',_'
   at_ Dans_arbre_pomme
   message_ 72
   done_

   11
   'nager' ',_'
   at_ Dans_arbre
   message_ 147
   anykey_
   goto_ Bord_lac
   desc_

   16
   'nager' ',_'
   at_ Sommet_arbre
   zero_ FLAG_POMME
   ink_ CLR_MAGENTA
   message_ 43
   ink_ INK_DEFAULT
   message_ 39
   done_

   11
   'nager' ',_'
   at_ Sommet_arbre
   message_ 75
   anykey_
   goto_ Bord_lac
   desc_

   8
   'nager' ',_'
   at_ Sommet_tour
   message_ 7
   done_

   8
   'suivre' 'papillon'
   present_ o_papillon
   message_ 120
   done_

   11
   'asseoir' ',_'
   at_ Etage_trone
   message_ 168
   anykey_
   goto_ Sur_trone
   desc_

   8
   'examiner' 'note'
   carried_ o_note
   message_ 21
   done_

   8
   'examiner' 'troll'
   at_ Dans_grotte
   message_ 10
   done_

   7
   'examiner' 'troll'
   at_ Devant_grotte
   message_ 10

   9
   'examiner' 'troll'
   at_ Devant_grotte
   zero_ FLAG_BOTTE
   message_ 68

   6
   'examiner' 'troll'
   at_ Devant_grotte
   done_

   8
   'examiner' 'grotte'
   at_ Devant_grotte
   message_ 60
   done_

   10
   'examiner' 'boîte'
   at_ Devant_grotte
   zero_ FLAG_CLE
   message_ 61
   done_

   8
   'examiner' 'botte'
   worn_ o_botte
   message_ 74
   done_

   8
   'examiner' 'botte'
   present_ o_botte
   message_ 73
   done_

   8
   'examiner' 'botte'
   present_ o_botte_vide
   message_ 77
   done_

   8
   'examiner' 'botte'
   carried_ o_botte_pleine
   message_ 108
   done_

   8
   'examiner' 'bouteille'
   present_ o_bouteille
   message_ 22
   done_

   21
   'examiner' 'nid'
   at_ Sommet_arbre
   notzero_ FLAG_NOTE
   clear_ FLAG_NOTE
   create_ o_note
   get_ o_note
   message_ 23
   anykey_
   cls_
   message_ 24
   anykey_
   desc_

   8
   'examiner' 'nid'
   at_ Sommet_arbre
   message_ 27
   done_

   8
   'examiner' 'nuage'
   at_ Sommet_arbre
   message_ 88
   done_

   14
   'examiner' 'nuage'
   at_ Sommet_tour
   zero_ FLAG_SCORE
   dropall_
   message_ 144
   anykey_
   goto_ etage_13
   desc_

   12
   'examiner' 'nuage'
   at_ Sommet_tour
   dropall_
   message_ 144
   anykey_
   goto_ Interieur_tour
   desc_

   8
   'examiner' 'mur'
   at_ Bord_lac
   message_ 37
   done_

   8
   'examiner' 'tour'
   present_ o_tour
   message_ 41
   done_

   8
   'examiner' 'tour'
   present_ o_tour_flamme
   message_ 26
   done_

   6
   'examiner' 'tour'
   message_ 202
   done_

   8
   'examiner' 'lac'
   at_ Bord_lac
   message_ 42
   done_

   8
   'examiner' 'eau'
   at_ Bord_lac
   message_ 42
   done_

   8
   'examiner' 'sable'
   at_ Bord_lac
   message_ 48
   done_

   8
   'examiner' 'poisson'
   present_ o_poisson
   message_ 94
   done_

   8
   'examiner' 'poisson'
   present_ o_poisson_mort
   message_ 70
   done_

   8
   'examiner' 'poisson'
   carried_ o_botte_pleine
   message_ 108
   done_

   10
   'examiner' 'pomme'
   at_ Foret
   present_ o_pomme_arbre
   message_ 49
   done_

   8
   'examiner' 'arbre'
   at_ Foret
   message_ 18
   done_

   8
   'examiner' 'graffiti'
   at_ Foret
   message_ 230
   done_

   8
   'examiner' 'souche'
   present_ o_souche
   message_ 31
   done_

   8
   'examiner' 'porte'
   present_ o_souche
   message_ 66
   done_

   8
   'examiner' 'herbe'
   at_ Clairiere
   message_ 69
   done_

   8
   'examiner' 'pomme'
   at_ Dans_arbre_pomme
   message_ 91
   done_

   8
   'examiner' 'branche'
   at_ Dans_arbre_pomme
   message_ 89
   done_

   8
   'examiner' 'arbre'
   at_ Dans_arbre_pomme
   message_ 89
   done_

   8
   'examiner' 'torche'
   present_ o_torche
   message_ 121
   done_

   8
   'examiner' 'torche'
   at_ Etage_trone
   message_ 153
   done_

   8
   'examiner' 'torche'
   at_ Sur_trone
   message_ 153
   done_

   8
   'examiner' 'coffre'
   present_ o_coffre_ouvert
   message_ 122
   done_

   11
   'examiner' 'trou'
   present_ o_trou_rat
   message_ 23
   anykey_
   goto_ Fake_room2
   desc_

   8
   'examiner' 'porte'
   at_ Interieur_tour
   message_ 138
   done_

   8
   'examiner' 'rat'
   present_ o_trou_rat
   message_ 148
   done_

   10
   'examiner' 'rat'
   at_ Sur_trone
   present_ o_rat_endormit
   message_ 250
   done_

   8
   'examiner' 'rat'
   present_ o_rat_endormit
   message_ 139
   done_

   8
   'examiner' 'rat'
   present_ o_rat_mort
   message_ 143
   done_

   8
   'examiner' 'yeux'
   present_ o_trou_rat
   message_ 148
   done_

   8
   'examiner' 'trône'
   at_ Etage_trone
   message_ 154
   done_

   8
   'examiner' 'trône'
   at_ Sur_trone
   message_ 154
   done_

   8
   'examiner' 'trognon'
   present_ o_trognon
   message_ 150
   done_

   8
   'examiner' 'clé'
   present_ o_cle
   message_ 78
   done_

   8
   'examiner' 'corbeau'
   at_ Sommet_tour
   message_ 162
   done_

   8
   'examiner' 'gargouille'
   at_ Sommet_tour
   message_ 156
   done_

   8
   'examiner' 'dé'
   at_ Etage_13
   message_ 167
   done_

   8
   'examiner' 'portrait'
   at_ Sur_trone
   message_ 110
   done_

   8
   'examiner' 'collier'
   at_ Sur_trone
   message_ 174
   done_

   8
   'examiner' 'perle'
   present_ o_perle
   message_ 183
   done_

   8
   'examiner' 'papillon'
   present_ o_papillon
   message_ 120
   done_

   8
   'examiner' 'squelette'
   present_ o_squelette
   message_ 192
   done_

   8
   'examiner' 'menotte'
   present_ o_squelette
   message_ 190
   done_

   8
   'examiner' 'poussière'
   present_ o_tas_poussiere
   message_ 194
   done_

   8
   'examiner' 'carte'
   present_ o_carte
   message_ 198
   done_

   8
   'examiner' 'croix'
   carried_ o_carte
   message_ 199
   done_

   8
   'examiner' 'pelle'
   present_ o_pelle
   message_ 115
   done_

   8
   'examiner' 'triton'
   present_ o_triton
   message_ 219
   done_

   8
   'examiner' 'grotte'
   at_ Dans_grotte
   message_ 235
   done_

   8
   'examiner' 'forêt'
   at_ Foret
   message_ 241
   done_

   8
   'examiner' 'cachot'
   at_ Cachot
   message_ 242
   done_

   8
   'examiner' ',_'
   at_ Obscurite_cachot
   message_ 104
   done_

   8
   'examiner' 'levier'
   at_ Etage_trone
   message_ 245
   done_

   8
   'examiner' 'levier'
   at_ Sur_trone
   message_ 245
   done_

   8
   'examiner' 'flamme'
   at_ cachot
   message_ 247
   done_

   8
   'examiner' 'mur'
   at_ cachot
   message_ 247
   done_

   10
   'examiner' 'fenêtre'
   atgt_ Sommet_tour
   atlt_ Etage_1
   message_ 253
   done_

   10
   'examiner' 'fenêtre'
   at_ Cachot
   absent_ o_squelette
   message_ 253
   done_

   6
   'examiner' ',_'
   message_ 8
   done_

   13
   'lire' 'note'
   carried_ o_note
   at_ Dans_arbre
   place_ o_note Bord_lac
   message_ 19
   done_

   13
   'lire' 'note'
   carried_ o_note
   at_ Dans_arbre_pomme
   place_ o_note Bord_lac
   message_ 19
   done_

   13
   'lire' 'note'
   carried_ o_note
   at_ Sommet_arbre
   place_ o_note Bord_lac
   message_ 19
   done_

   12
   'lire' 'note'
   carried_ o_note
   destroy_ o_note
   set_ FLAG_NOTE
   message_ 32
   done_

   10
   'lire' 'carte'
   carried_ o_carte
   set_ FLAG_CARTE
   message_ 199
   done_

   8
   'lire' 'carte'
   at_ Cachot
   message_ 125
   done_

   6
   'fouiller' ',_'
   message_ 109
   done_

   20
   'prendre' 'botte'
   at_ Devant_grotte
   zero_ FLAG_BOTTE
   set_ FLAG_BOTTE
   get_ o_botte
   set_ FLAG_CORBEAU
   message_ 12
   anykey_
   picture_ 9 Devant_grotte
   desc_

   8
   'prendre' 'botte'
   present_ o_botte
   get_ o_botte
   desc_

   8
   'prendre' 'botte'
   present_ o_botte_vide
   get_ o_botte_vide
   desc_

   10
   'prendre' 'clé'
   at_ Devant_grotte
   zero_ FLAG_CLE
   message_ 63
   done_

   24
   'prendre' 'bouteille'
   present_ o_bouteille
   destroy_ o_bouteille
   create_ o_note
   get_ o_note
   clear_ FLAG_NOTE
   picture_ 23 Foret
   message_ 23
   anykey_
   cls_
   message_ 25
   anykey_
   desc_

   9
   'prendre' 'nid'
   message_ 87
   anykey_
   goto_ Foret
   desc_

   13
   'prendre' 'pomme'
   at_ Dans_arbre_pomme
   set_ FLAG_NAGE
   message_ 52
   anykey_
   goto_ Dans_lac
   desc_

   8
   'prendre' 'eau'
   at_ Bord_lac
   message_ 45
   done_

   8
   'prendre' 'sable'
   at_ Bord_lac
   message_ 47
   done_

   10
   'prendre' 'pomme'
   at_ Foret
   present_ o_pomme_arbre
   message_ 50
   done_

   8
   'prendre' 'herbe'
   at_ Clairiere
   message_ 71
   done_

   10
   'prendre' 'poisson'
   at_ Bord_lac
   present_ o_poisson
   message_ 95
   done_

   8
   'prendre' 'poisson'
   carried_ o_botte_pleine
   message_ 118
   done_

   8
   'prendre' 'rat'
   present_ o_trou_rat
   message_ 163
   done_

   10
   'prendre' 'rat'
   at_ Sur_trone
   present_ o_rat_mort
   message_ 143
   done_

   8
   'prendre' 'torche'
   at_ Etage_trone
   message_ 151
   done_

   8
   'prendre' 'corbeau'
   at_ Sommet_tour
   message_ 158
   done_

   8
   'prendre' 'torche'
   at_ Sur_trone
   message_ 172
   done_

   8
   'prendre' 'portrait'
   at_ Sur_trone
   message_ 173
   done_

   8
   'prendre' 'collier'
   at_ Sur_trone
   message_ 173
   done_

   8
   'prendre' 'coffre'
   at_ Dans_grotte
   message_ 185
   done_

   8
   'prendre' 'papillon'
   present_ o_papillon
   message_ 186
   done_

   8
   'prendre' 'squelette'
   present_ o_squelette
   message_ 195
   done_

   8
   'prendre' 'poussière'
   present_ o_tas_poussiere
   message_ 195
   done_

   8
   'prendre' 'menotte'
   present_ o_squelette
   message_ 197
   done_

   14
   'prendre' 'carte'
   zero_ FLAG_INTERLUDE
   present_ o_carte
   notcarr_ o_carte
   set_ FLAG_INTERLUDE
   cls_
   message_ 206

   15
   'prendre' 'carte'
   present_ o_carte
   notcarr_ o_carte
   zero_ FLAG_PERLE
   destroy_ o_perle
   destroy_ o_poisson_mort
   message_ 208

   11
   'prendre' 'carte'
   present_ o_carte
   notcarr_ o_carte
   notzero_ FLAG_PERLE
   message_ 207

   13
   'prendre' 'carte'
   present_ o_carte
   notcarr_ o_carte
   set_ FLAG_INTERLUDE
   get_ o_carte
   anykey_
   desc_

   6
   'prendre' 'tout'
   message_ 46
   done_

   5
   'prendre' ',_'
   autog_
   desc_

   8
   'poser' 'note'
   carried_ o_note
   message_ 21
   done_

   8
   'poser' 'botte'
   worn_ o_botte
   message_ 80
   done_

   8
   'poser' 'botte'
   present_ o_botte
   drop_ o_botte
   desc_

   8
   'poser' 'botte'
   present_ o_botte_vide
   drop_ o_botte_vide
   desc_

   8
   'poser' 'botte'
   carried_ o_botte_pleine
   message_ 100
   done_

   8
   'poser' 'pomme'
   at_ Obscurite_cachot
   message_ 107
   done_

   6
   'poser' 'tout'
   message_ 46
   done_

   5
   'poser' ',_'
   autod_
   desc_

   10
   'donner' 'rat'
   at_ Sommet_tour
   carried_ o_rat_endormit
   message_ 140
   done_

   10
   'donner' 'trognon'
   at_ Sommet_tour
   carried_ o_trognon
   message_ 140
   done_

   10
   'donner' 'trognon'
   present_ o_trou_rat
   carried_ o_trognon
   message_ 6
   done_

   10
   'montrer' 'rat'
   at_ Sommet_tour
   carried_ o_rat_endormit
   message_ 140
   done_

   10
   'montrer' 'trognon'
   at_ Sommet_tour
   carried_ o_trognon
   message_ 140
   done_

   10
   'montrer' 'trognon'
   present_ o_trou_rat
   carried_ o_trognon
   message_ 6
   done_

   10
   'lancer' 'dé'
   at_ Etage_13
   notcarr_ o_de100
   message_ 125
   done_

   12
   'lancer' 'dé'
   carried_ o_de100
   zero_ FLAG_SCORE
   chance_ 19
   let_ FLAG_SCORE 20

   12
   'lancer' 'dé'
   carried_ o_de100
   zero_ FLAG_SCORE
   chance_ 39
   let_ FLAG_SCORE 40

   12
   'lancer' 'dé'
   carried_ o_de100
   zero_ FLAG_SCORE
   chance_ 59
   let_ FLAG_SCORE 60

   12
   'lancer' 'dé'
   carried_ o_de100
   zero_ FLAG_SCORE
   chance_ 79
   let_ FLAG_SCORE 80

   12
   'lancer' 'dé'
   carried_ o_de100
   zero_ FLAG_SCORE
   chance_ 99
   let_ FLAG_SCORE 100

   15
   'lancer' 'dé'
   carried_ o_de100
   destroy_ o_de100
   cls_
   message_ 145
   score_
   anykey_
   goto_ Interieur_tour
   desc_

   6
   'lancer' ',_'
   message_ 252
   done_

   21
   'insérer' 'note'
   at_ Devant_grotte
   carried_ o_note
   zero_ FLAG_PASSAGE
   set_ FLAG_CLE
   destroy_ o_note
   place_ o_cle Dans_grotte
   message_ 33
   message_ 35
   done_

   20
   'insérer' 'note'
   at_ Devant_grotte
   carried_ o_note
   set_ FLAG_CLE
   destroy_ o_note
   create_ o_cle
   get_ o_cle
   message_ 34
   message_ 35
   done_

   10
   'insérer' 'perle'
   at_ Dans_grotte
   notcarr_ o_perle
   message_ 125
   done_

   21
   'insérer' 'perle'
   at_ Dans_grotte
   carried_ o_perle
   destroy_ o_perle
   swap_ o_coffre_ouvert o_coffre_ferme
   picture_ 33 Dans_grotte
   set_ FLAG_PERLE
   message_ 124
   anykey_
   desc_

   20
   'insérer' 'trognon'
   carried_ o_trognon
   present_ o_trou_rat
   destroy_ o_trou_rat
   swap_ o_trognon o_rat_endormit
   picture_ 17 Interieur_tour
   cls_
   message_ 132
   anykey_
   desc_

   8
   'insérer' ',_'
   at_ Dans_grotte
   message_ 123
   done_

   10
   'insérer' 'note'
   at_ Devant_grotte
   notcarr_ o_note
   message_ 125
   done_

   8
   'insérer' ',_'
   at_ Devant_grotte
   message_ 114
   done_

   10
   'mettre' 'botte'
   carried_ o_botte
   wear_ o_botte
   message_ 81
   done_

   8
   'mettre' 'botte'
   carried_ o_botte_vide
   message_ 76
   done_

   5
   'mettre' ',_'
   autow_
   ok_

   8
   'enlever' 'botte'
   worn_ o_botte
   message_ 80
   done_

   15
   'enlever' 'menotte'
   present_ o_squelette
   message_ 193
   anykey_
   swap_ o_squelette o_tas_poussiere
   picture_ 27 cachot
   desc_

   5
   'enlever' ',_'
   autor_
   ok_

   8
   'remplir' 'botte'
   carried_ o_botte
   message_ 79
   done_

   21
   'remplir' 'botte'
   at_ Bord_lac
   carried_ o_botte_vide
   present_ o_poisson
   destroy_ o_poisson
   swap_ o_botte_vide o_botte_pleine
   picture_ 25 Bord_lac
   message_ 96
   anykey_
   desc_

   12
   'remplir' 'botte'
   at_ Bord_lac
   carried_ o_botte_vide
   chance_ 50
   message_ 85
   done_

   10
   'remplir' 'botte'
   at_ Bord_lac
   carried_ o_botte_vide
   message_ 86
   done_

   8
   'remplir' 'botte'
   carried_ o_botte_vide
   message_ 84
   done_

   8
   'remplir' 'triton'
   carried_ o_triton
   message_ 221
   done_

   19
   'vider' 'botte'
   carried_ o_botte_pleine
   at_ Bord_lac
   create_ o_poisson
   swap_ o_botte_pleine o_botte_vide
   message_ 97
   anykey_
   picture_ 26 Bord_lac
   desc_

   13
   'vider' 'botte'
   carried_ o_botte_pleine
   destroy_ o_botte_pleine
   create_ o_poisson_mort
   message_ 99
   anykey_
   desc_

   8
   'vider' 'triton'
   carried_ o_triton
   message_ 220
   done_

   8
   'manger' 'pomme'
   at_ Dans_arbre_pomme
   message_ 125
   done_

   8
   'manger' 'pomme'
   present_ o_pomme_arbre
   message_ 125
   done_

   17
   'manger' 'pomme'
   at_ Obscurite_cachot
   swap_ o_pomme o_trognon
   set_ FLAG_TROGNON
   cls_
   message_ 92
   anykey_
   goto_ Interieur_tour
   desc_

   8
   'manger' 'trognon'
   present_ o_trognon
   message_ 149
   done_

   8
   'manger' 'herbe'
   at_ Clairiere
   message_ 125
   done_

   14
   'manger' 'poisson'
   carried_ o_poisson_mort
   destroy_ o_poisson_mort
   create_ o_perle
   cls_
   message_ 182
   anykey_
   desc_

   8
   'manger' 'poisson'
   present_ o_poisson_mort
   message_ 125
   done_

   8
   'manger' 'rat'
   present_ o_rat_endormit
   message_ 141
   done_

   8
   'manger' 'rat'
   present_ o_rat_mort
   message_ 149
   done_

   8
   'manger' 'triton'
   present_ o_triton
   message_ 220
   done_

   14
   'boire' 'eau'
   at_ Bord_lac
   ink_ CLR_MAGENTA
   message_ 43
   ink_ INK_DEFAULT
   message_ 44
   done_

   8
   'ouvrir' 'boîte'
   at_ Devant_grotte
   message_ 62
   done_

   27
   'ouvrir' 'porte'
   present_ o_souche
   carried_ o_cle
   picture_ 35 Clairiere
   picture_ 24 Foret
   place_ o_papillon Foret
   destroy_ o_cle
   swap_ o_souche o_souche_abandonnee
   message_ 180
   anykey_
   goto_ Fake_room3
   desc_

   8
   'ouvrir' 'porte'
   present_ o_souche
   message_ 65
   done_

   14
   'ouvrir' 'porte'
   at_ Interieur_tour
   zero_ FLAG_BUG
   set_ FLAG_BUG
   cls_
   message_ 136
   anykey_
   desc_

   8
   'ouvrir' 'porte'
   at_ Interieur_tour
   message_ 137
   done_

   8
   'ouvrir' 'coffre'
   present_ o_coffre_ferme
   message_ 178
   done_

   15
   'ouvrir' 'menotte'
   present_ o_squelette
   message_ 193
   anykey_
   swap_ o_squelette o_tas_poussiere
   picture_ 27 cachot
   desc_

   16
   'ouvrir' 'poisson'
   carried_ o_poisson_mort
   destroy_ o_poisson_mort
   create_ o_perle
   get_ o_perle
   cls_
   message_ 209
   anykey_
   desc_

   8
   'ouvrir' 'poisson'
   present_ o_poisson_mort
   message_ 125
   done_

   27
   'déverrouiller' 'porte'
   present_ o_souche
   carried_ o_cle
   picture_ 35 Clairiere
   picture_ 24 Foret
   place_ o_papillon Foret
   destroy_ o_cle
   swap_ o_souche o_souche_abandonnee
   message_ 180
   anykey_
   goto_ Fake_room3
   desc_

   8
   'déverrouiller' 'porte'
   present_ o_souche
   message_ 65
   done_

   8
   'déverrouiller' 'porte'
   at_ Interieur_tour
   message_ 137
   done_

   8
   'déverrouiller' 'coffre'
   present_ o_coffre_ferme
   message_ 178
   done_

   11
   'déverrouiller' 'menotte'
   present_ o_squelette
   message_ 237
   let_ FLAG_ESSAI 4
   done_

   17
   'code' 'f=gm1m2/r2'
   present_ o_squelette
   notzero_ FLAG_ESSAI
   message_ 240
   anykey_
   swap_ o_squelette o_tas_poussiere
   picture_ 27 cachot
   desc_

   8
   'code' ',_'
   notzero_ FLAG_ESSAI
   message_ 238
   done_

   8
   'fermer' 'coffre'
   present_ o_coffre_ouvert
   message_ 177
   done_

   8
   'pousser' 'troll'
   at_ Devant_grotte
   message_ 58
   done_

   8
   'pousser' 'troll'
   at_ Dans_grotte
   message_ 58
   done_

   8
   'pousser' 'trône'
   at_ Etage_trone
   message_ 155
   done_

   8
   'pousser' 'coffre'
   at_ Dans_grotte
   message_ 185
   done_

   10
   'pousser' 'torche'
   at_ Etage_trone
   cls_
   message_ 152
   anykey_
   desc_

   8
   'pousser' 'torche'
   at_ Sur_trone
   message_ 172
   done_

   10
   'pousser' 'levier'
   at_ Etage_trone
   cls_
   message_ 152
   anykey_
   desc_

   8
   'pousser' 'levier'
   at_ Sur_trone
   message_ 172
   done_

   8
   'casser' 'mur'
   at_ Bord_lac
   message_ 38
   done_

   8
   'casser' 'arbre'
   at_ Foret
   message_ 51
   done_

   8
   'casser' 'branche'
   at_ Dans_arbre_pomme
   message_ 72
   done_

   6
   'casser' ',_'
   message_ 7
   done_

   8
   'frapper' 'porte'
   at_ Clairiere
   message_ 67
   done_

   8
   'frapper' 'porte'
   at_ Interieur_tour
   message_ 117
   done_

   21
   'creuser' ',_'
   at_ Bord_lac
   carried_ o_pelle
   notzero_ FLAG_CARTE
   destroy_ o_pelle
   create_ o_triton
   cls_
   message_ 205
   anykey_
   picture_ 29 Bord_lac
   desc_

   10
   'creuser' ',_'
   at_ Bord_lac
   carried_ o_pelle
   message_ 203
   done_

   8
   'creuser' ',_'
   at_ Bord_lac
   message_ 47
   done_

   8
   'creuser' 'poussière'
   present_ o_tas_poussiere
   message_ 195
   done_

   6
   'creuser' ',_'
   message_ 204
   done_

   8
   'allumer' 'torche'
   carried_ o_torche
   message_ 184
   done_

   8
   'lumos' ',_'
   at_ Obscurite_cachot
   goto_ Fake_room4
   desc_

   6
   'lumos' ',_'
   message_ 214
   done_

   8
   'nox' ',_'
   present_ o_squelette
   goto_ Fake_room5
   desc_

   6
   'nox' ',_'
   message_ 214
   done_

   8
   'éteindre' 'torche'
   at_ Etage_trone
   message_ 3
   done_

   8
   'éteindre' 'torche'
   at_ Sur_trone
   message_ 172
   done_

   8
   'éteindre' 'flamme'
   at_ Cachot
   message_ 3
   done_

   8
   'souffler' 'torche'
   at_ Etage_trone
   message_ 244
   done_

   8
   'souffler' 'torche'
   at_ Sur_trone
   message_ 172
   done_

   8
   'souffler' 'flamme'
   at_ Etage_trone
   message_ 244
   done_

   8
   'souffler' 'flamme'
   at_ Sur_trone
   message_ 172
   done_

   8
   'souffler' 'flamme'
   at_ Cachot
   message_ 244
   done_

   16
   'souffler' 'poussière'
   present_ o_tas_poussiere
   cls_
   message_ 196
   anykey_
   picture_ 37 Cachot
   swap_ o_tas_poussiere o_carte
   desc_

   10
   'souffler' 'triton'
   present_ o_triton
   notcarr_ o_triton
   message_ 125
   done_

   12
   'souffler' 'triton'
   at_ Devant_grotte
   carried_ o_triton
   cls_
   message_ 225
   anykey_
   cls_

   13
   'souffler' 'triton'
   at_ Devant_grotte
   carried_ o_triton
   zero_ FLAG_PERLE
   message_ 251
   anykey_
   cls_

   13
   'souffler' 'triton'
   at_ Devant_grotte
   carried_ o_triton
   notzero_ FLAG_PERLE
   message_ 227
   anykey_
   cls_

   11
   'souffler' 'triton'
   at_ Devant_grotte
   carried_ o_triton
   zero_ FLAG_PERLE
   message_ 228

   11
   'souffler' 'triton'
   at_ Devant_grotte
   carried_ o_triton
   notzero_ FLAG_PERLE
   message_ 229

   11
   'souffler' 'triton'
   at_ Devant_grotte
   carried_ o_triton
   anykey_
   goto_ Epilogue
   desc_

   10
   'souffler' 'triton'
   at_ Bord_lac
   carried_ o_triton
   message_ 215
   done_

   9
   'souffler' 'triton'
   at_ Foret
   carried_ o_triton
   message_ 216

   17
   'souffler' 'triton'
   at_ Foret
   carried_ o_triton
   zero_ FLAG_BEBE
   set_ FLAG_BEBE
   anykey_
   cls_
   message_ 217
   anykey_
   desc_

   8
   'souffler' 'triton'
   at_ Foret
   carried_ o_triton
   done_

   10
   'souffler' 'triton'
   at_ Dans_grotte
   carried_ o_triton
   message_ 223
   done_

   10
   'souffler' 'triton'
   at_ Clairiere
   carried_ o_triton
   message_ 222
   done_

   6
   'souffler' ',_'
   message_ 146
   done_

   10
   'brûler' 'carte'
   carried_ o_carte
   zero_ FLAG_CARTE
   message_ 21
   done_

   19
   'brûler' 'carte'
   carried_ o_carte
   cls_
   message_ 200
   anykey_
   picture_ 28 Bord_lac
   destroy_ o_tour_flamme
   destroy_ o_carte
   goto_ Bord_lac
   desc_

   8
   'brûler' 'carte'
   present_ o_carte
   message_ 125
   done_

   8
   'écouter' ',_'
   at_ Sommet_tour
   message_ 20
   done_

   8
   'écouter' ',_'
   at_ Sur_trone
   message_ 20
   done_

   10
   'écouter' ',_'
   atgt_ Interieur_tour
   atlt_ Cachot
   message_ 56
   done_

   10
   'écouter' ',_'
   atgt_ Prologue_suite
   atlt_ Dans_lac
   message_ 9
   done_

   6
   'écouter' ',_'
   message_ 5
   done_

   8
   'sentir' 'pomme'
   at_ Dans_arbre_pomme
   message_ 90
   done_

   8
   'sentir' 'pomme'
   at_ Obscurite_cachot
   message_ 90
   done_

   8
   'sentir' 'botte'
   carried_ o_botte
   message_ 82
   done_

   8
   'sentir' 'rat'
   present_ o_rat_endormit
   message_ 103
   done_

   8
   'sentir' 'rat'
   present_ o_rat_mort
   message_ 188
   done_

   8
   'sentir' 'poisson'
   present_ o_poisson_mort
   message_ 188
   done_

   8
   'sentir' ',_'
   at_ Obscurite_cachot
   message_ 218
   done_

   6
   'sentir' ',_'
   message_ 11
   done_

   8
   'parler' 'arbre'
   at_ Foret
   message_ 133
   done_

   8
   'parler' 'troll'
   at_ Devant_grotte
   message_ 29
   done_

   8
   'parler' 'amour'
   at_ Devant_grotte
   message_ 30
   done_

   8
   'parler' 'corbeau'
   at_ Sommet_tour
   message_ 160
   done_

   8
   'parler' 'squelette'
   at_ Cachot
   message_ 189
   done_

   6
   'parler' ',_'
   message_ 175
   done_

   8
   'appeler' 'corbeau'
   at_ Sommet_tour
   message_ 160
   done_

   8
   'appeler' 'corbeau'
   at_ Sur_trone
   message_ 160
   done_

   6
   'appeler' 'corbeau'
   message_ 175
   done_

   10
   'croa' ',_'
   at_ Sommet_tour
   message_ 161
   message_ 20
   done_

   20
   'croa' ',_'
   at_ Sur_trone
   present_ o_rat_mort
   cls_
   message_ 113
   anykey_
   place_ o_poisson Bord_lac
   picture_ 26 Bord_lac
   goto_ Dans_grotte
   desc_

   10
   'croa' ',_'
   at_ Sur_trone
   cls_
   message_ 112
   anykey_
   desc_

   8
   'croa' ',_'
   at_ Etage_trone
   message_ 246
   done_

   6
   'croa' ',_'
   message_ 175
   done_

   12
   'tuer' 'rat'
   at_ Sur_trone
   present_ o_rat_endormit
   notcarr_ o_rat_endormit
   message_ 125
   done_

   15
   'tuer' 'rat'
   at_ Sur_trone
   carried_ o_rat_endormit
   destroy_ o_rat_endormit
   create_ o_rat_mort
   message_ 142
   anykey_
   desc_

   8
   'tuer' 'rat'
   present_ o_rat_endormit
   message_ 141
   done_

   8
   'tuer' 'corbeau'
   at_ Sommet_tour
   message_ 141
   done_

   8
   'tuer' 'troll'
   at_ Devant_grotte
   message_ 187
   done_

   8
   'tuer' 'troll'
   at_ Dans_grotte
   message_ 187
   done_

   8
   'tuer' 'papillon'
   present_ o_papillon
   message_ 141
   done_

   8
   'tuer' 'poisson'
   present_ o_poisson
   message_ 141
   done_

   6
   'embrasser' 'squelette'
   message_ 191
   done_

   8
   'embrasser' 'rat'
   present_ o_rat_endormit
   message_ 210
   done_

   8
   'embrasser' 'troll'
   at_ Devant_grotte
   message_ 234
   done_

   8
   'embrasser' 'troll'
   at_ Dans_grotte
   message_ 234
   done_

   10
   'réveiller' 'rat'
   at_ Sur_trone
   present_ o_rat_endormit
   message_ 249
   done_

   8
   'réveiller' 'rat'
   present_ o_rat_endormit
   message_ 210
   done_

   8
   'caresser' 'rat'
   carried_ o_rat_endormit
   message_ 210
   done_

   11
   'caresser' 'troll'
   at_ Devant_grotte
   message_ 233
   let_ FLAG_SCORE 100
   done_

   8
   'caresser' 'troll'
   at_ Dans_grotte
   message_ 232
   done_

   8
   'caresser' 'squelette'
   carried_ o_squelette
   message_ 195
   done_

   8
   'penser' ',_'
   at_ Sur_trone
   message_ 171
   done_

   6
   'penser' ',_'
   message_ 169
   done_

   8
   'attendre' ',_'
   at_ Fond_lac
   goto_ Fond_lac1
   desc_

   8
   'attendre' ',_'
   at_ Fond_lac_98
   goto_ Fond_lac1
   desc_

   8
   'attendre' ',_'
   at_ Fond_lac_99
   goto_ Fond_lac1
   desc_

   17
   'attendre' ',_'
   at_ Obscurite_cachot
   zero_ FLAG_REVE
   set_ FLAG_REVE
   message_ 170
   anykey_
   cls_
   message_ 231
   anykey_
   desc_

   12
   'attendre' ',_'
   at_ Sommet_tour
   dropall_
   message_ 144
   anykey_
   goto_ Interieur_tour
   desc_

   6
   'attendre' ',_'
   message_ 4
   done_

   17
   'dormir' ',_'
   at_ Obscurite_cachot
   zero_ FLAG_REVE
   set_ FLAG_REVE
   message_ 170
   anykey_
   cls_
   message_ 231
   anykey_
   desc_

   8
   'dormir' ',_'
   at_ Obscurite_cachot
   message_ 170
   done_

   6
   'dormir' ',_'
   message_ 211
   done_

   6
   'utiliser' ',_'
   message_ 1
   done_

   8
   'aide' ',_'
   cls_
   message_ 28
   anykey_
   desc_

   9
   'propos' ',_'
   cls_
   message_ 131
   version_
   anykey_
   desc_

   4
   'inventaire' ',_'
   inven_

   10
   'regarder' 'fenêtre'
   atgt_ Sommet_tour
   atlt_ Etage_1
   message_ 179
   done_

   10
   'regarder' 'fenêtre'
   at_ Cachot
   absent_ o_squelette
   message_ 179
   done_

   4
   'regarder' ',_'
   desc_

   5
   'score' ',_'
   score_
   done_

   7
   'quitter' ',_'
   quit_
   score_
   turns_
   end_

   4
   'sauver' ',_'
   save_

   4
   'charger' ',_'
   load_

   5
   'script' ',_'
   script_
   done_

   5
   'version' ',_'
   version_
   done_

   0
;

