Constant CAPACITY 4;
Constant LIGHT_OBJECT o_torche_allumee;

!not_created
!at_worn
!at_carried

Object o_tour
with
   describe "La tour du Corbeau vous observe.",
   at Bord_lac,
has female;

Object o_tour_flamme
with
   describe "La tour du Corbeau est en flamme.",
   at not_created,
has female;

Object o_trou_rat
with
   describe "Un trou à rat.",
   at Interieur_tour,
has male;

Object o_rat_mort
with
   describe "Un rat mort étendu au sol.",
   at not_created,
has male;

Object o_pomme_arbre
with
   describe "Une pomme accrochée à un arbre.",
   at Foret,
has female;

Object o_bouteille
with
   describe "Une bouteille flotte dans l'eau du lac.",
   at not_created,
has female;

Object o_boite_lettre
with
   describe "Une boîte aux lettres taillée dans le roc.",
   at Devant_grotte,
has female;

Object o_souche
with
   describe "Une curieuse souche pourvue d'une porte extérieur.",
   at Clairiere,
has female;

Object o_souche_abandonnee
with
   describe "Une souche abandonnée vouée à la décomposition.",
   at not_created,
has female;

Object o_torche_allumee
with
   describe "Une torche allumée scellée au mur.",
   at Etage_trone,
has female;

Object o_trone
with
   describe "Un trône en bois sculpté.",
   at Etage_trone,
has male;

Object o_poisson
with
   describe "Un petit poisson joue à faire des sauts dans l'eau.",
   at not_created,
has male;

Object o_coffre_ouvert
with
   describe "Un petit coffre à secrets grand ouvert.",
   at Dans_grotte,
has male;

Object o_coffre_ferme
with
   describe "Un petit coffre à secrets refermé sur lui-même.",
   at not_created,
has male;

Object o_papillon
with
   describe "Un papillon posé sur un tronc d'arbre.",
   at not_created,
has male;

Object o_squelette
with
   describe "Un squelette menotté au mur d'un sinistre cachot.",
   at Cachot,
has male;

Object o_tas_poussiere
with
   describe "Un tas de poussière.",
   at not_created,
has male;

Object o_torche
with
   describe "* Une torche éteinte",
   name 'torche',
   at Dans_grotte,
has female;

Object o_botte
with
   describe "* Une botte écarlate",
   name 'botte',
   at Devant_grotte,
has female;

Object o_botte_vide
with
   describe "* Une vieille botte usée",
   name 'botte',
   at not_created,
has female;

Object o_botte_pleine
with
   describe "* Une botte remplie d'eau du lac",
   name 'botte',
   at not_created,
has female;

Object o_de100
with
   describe "* Un dé à cent faces",
   name 'dé',
   at Etage_13,
has male;

Object o_poisson_mort
with
   describe "* Un poisson mort",
   name 'poisson',
   at not_created,
has male;

Object o_cle
with
   describe "* Une petite clé",
   name 'clé',
   at not_created,
has female;

Object o_carte
with
   describe "* Une carte au trésor",
   name 'carte',
   at not_created,
has female;

Object o_note
with
   describe "* Une note de service",
   name 'note',
   at at_carried,
has female;

Object o_perle
with
   describe "* Une perle noire",
   name 'perle',
   at not_created,
has female;

Object o_pomme
with
   describe "* Une pomme mûre",
   at not_created,
has female;

Object o_trognon
with
   describe "* Un trognon de pomme",
   name 'trognon',
   at not_created,
has male;

Object o_triton
with
   describe "* Un triton conque",
   name 'triton',
   at not_created,
has male;

Object o_pelle
with
   describe "* Une pelle en plastique",
   name 'pelle',
   at Dans_grotte,
has female;

Object o_rat_endormit
with
   describe "* Un rat endormit Zzzz",
   name 'rat',
   at not_created,
has male;

