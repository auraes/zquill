Array messages-->
!0
   ""
!1
   "Utilisez un verbe moins générique."
!2
   "Utilisez les points cardinaux ou d'autres verbes de déplacement."
!3
   "Il n'y a pas d'extincteur fonctionnel facilement accessible. Un blâme pour le level designer."
!4
   "... Un peu plus tard."
!5
   "Vous n'entendez rien de particulier."
!6
   "Le rat jette un œil à l'offrande et déglutit sa salive tout en restant caché."
!7
   "Vous pourriez vous blesser."
!8
   "Vous ne remarquez rien de particulier."
!9
   "Quelques bruits épars, mais où est donc passée la vie sauvage ?"
!10
   "Un géant de pierre qui n'est plus dangereux pour personne, mais l'était-il vraiment ?"
!11
   "Certaines odeurs réveillent des souvenirs les plus anciens."
!12
   "Après maints efforts, vous parvenez à retirer la botte du pied du troll."
!13
   "Ne vous promenez pas en forêt sans être accompagné(e) d'un guide."
!14
   "Évaluation du saut vertical : 100 %"
!15
   "Évaluation du saut vertical : 75 %"
!16
   "Une échelle est indispensable, mais le jeu en est dépourvu."
!17
   "^Une bouteille flotte dans le lac."
!18
   "Deux arbres du paradis portant la ramure haute et fière. Vous remarquez un graffiti gravé sur l'un d'eux."
!19
   "Une rafale de vent vous l'arrache des mains et l'emporte au loin."
!20
   "^CROÂ CROÂ ..."
!21
   "Vous devriez d'abord la LIRE."
!22
   "Elle provient probablement des poubelles de l'Ancien Monde."
!23
   "Voyons voir ce qu'il y a à l'intérieur...^"
!24
   "Oh ! C'est votre indispensable note de service.^Vous la récupérerez et laissez le nid douillet pour les générations futures."
!25
   "Oh ! C'est votre indispensable note de service.^Vous la récupérerez et jetez la bouteille dans les profondeurs du lac."
!26
   "Au nord, de l'autre côté du lac, la tour du Corbeau en proie aux flammes s'impatiente. Elle vous attend."
!27
   "Le nid est vide. Le gentil bébé oiseau a dû être mangé par un gros vilain prédateur."
!28
   "Saisissez vos commandes, en un ou deux mots, pour interagir et progresser dans le jeu. Le caractère * peut se substituer au second terme.^^Déplacez-vous selon les points cardinaux (n, s, e, o) ou les verbes de déplacement : monter (m), descendre (d), entrer, sortir^Regarder (r, l) pour rafraîchir l'écran^Inventaire (i) pour afficher son contenu^Sauver, charger, script, quitter^Aide, à propos^^Quelques verbes utiles pour terminer le jeu :^Examiner (x), Lire^Grimper, sauter, s'asseoir, se lever^Prendre, poser, insérer, lancer, tirer, mettre^Remplir, vider, manger^Ouvrir, creuser, souffler, brûler, tuer^Parler, Croâ^Attendre"
!29
   "Il fait la sourde oreille."
!30
   "Un long frisson parcourt l'épiderme minéral du troll."
!31
   "Une souche creuse vraisemblablement habitée par un lutin facétieux."
!32
   "Il est écrit : Si vous avez besoin d'instructions, tapez AIDE. Tapez À PROPOS pour en savoir un peu plus sur le jeu.^^La note de service se transforme en avion de papier et s'envole au loin."
!33
   "Vous parvenez sans peine à déloger la clé qui glisse vers l'intérieur de la grotte."
!34
   "Vous parvenez sans peine à déloger la clé que vous récupérez."
!35
   "^La note de service se transforme en oiseau de feu et disparaît à jamais."
!36
   "Paf ! Vous vous heurtez à un mur invisible. Un blâme pour le level designer."
!37
   "En somme, vous n'êtes qu'une brique invisible de plus dans le mur."
!38
   "Interagir avec l'invisible est l'expérience intérieure ultime."
!39
   "Il n'est jamais trop tard pour apprendre à NAGER."
!40
   "Vous trempez vos pieds dans l'eau et cela vous suffit."
!41
   "Au nord, de l'autre côté du lac, la tour du Corbeau s'impatiente. Elle vous attend."
!42
   "De l'eau froide, profonde et de couleur changeante, où se reflète le paysage."
!43
   "Prévention des risques :"
!44
   "Ne laissez pas vos enfants boire n'importe quoi !"
!45
   "L'eau dégoutte lentement de vos mains."
!46
   "Une chose après l'autre."
!47
   "Vous prenez une poignée de sable et la jetez au vent."
!48
   "Une étendue de sable fin et chaud dissimulant des trésors à la pelle."
!49
   "Ce fruit du paradis vous surplombe ostensiblement."
!50
   "Haut perchée, la pomme est hors d'atteinte."
!51
   "Prenez garde, leur colère s'enracine en enfer."
!52
   "La branche qui vous porte casse subitement. Vous basculez dans le lac..."
!53
   "^La note de service se transforme en sous-marin de poche et disparaît au loin."
!54
   "Avant de faire le grand saut, échauffez-vous."
!55
   "Évaluation du saut vertical : 23 %"
!56
   "Le silence éternel des espaces infinis."
!57
   "Alors que vous franchissez le seuil, la porte se referme brutalement derrière vous.^^La tour du Corbeau vous a capturé(e) comme un loup affamé."
!58
   "Le colosse de pierre ne bouge pas d'un pouce."
!59
   "Le sol se dérobe sous vos pieds, vous chutez... L'espace et le temps, unis à jamais, dansent pour vous.^^Mais où est donc passé le niveau inférieur 13 ?^^La voix, les larmes aux yeux, s'approche de vous et vous dit : Vous savez quoi ? Je suis heureuse. Ah ! au fait, je vous ai ramené quelque chose."
!60
   "Vous voyez fort bien comment on y entre, mais pas comment on en sort."
!61
   "Il y a une petite clé coincée à l'intérieur, à proximité de la fente d'insertion des lettres et des prospectus."
!62
   "Ce n'est qu'une fente d'insertion du courrier percée grossièrement dans le rocher."
!63
   "L'ouverture est trop étroite pour vos gros doigts maladroits."
!64
   "Nul doute que vous aurez bientôt une statue à votre effigie sur ce piédestal en bois mort."
!65
   "Une clé sécurisée est indispensable pour entrer sans être invité."
!66
   "Une petite porte en bois munie d'une serrure à clé pour plus de sécurité et de tranquillité d'esprit."
!67
   "Toc toc ! Un grand frou-frou d'ailes agitées résonne au creux de la souche."
!68
   "Il a une belle botte rouge au pied."
!69
   "En l'absence des grands herbivores, la végétation prospère."
!70
   "Sa mort prématurée est probablement due à une fausse-route."
!71
   "Le prélèvement de spécimens d'espèce de la flore sauvage est interdit."
!72
   "Soyez un peu patient, cela ne saurait tarder."
!73
   "La botte semble fort grande et fort large, mais elle est fée. L'usage que vous pourriez en faire saute aux yeux."
!74
   "La botte est confortable et le rouge vous va très bien : il vous met en valeur."
!75
   "Vlouff ! Vous faites un plongeon acrobatique de haut-vol, sans personne pour vous acclamer."
!76
   "Elle est beaucoup trop grande et trop large pour votre pied."
!77
   "Le charme a disparu et rien ne saurait combler le vide de son absence."
!78
   "C'est une clé fée ayant appartenu à Barbe bleue ou l'un des siens."
!79
   "Il n'y a qu'une seule chose qui puisse la combler."
!80
   "Un tire-botte est indispensable, mais le jeu en est dépourvu."
!81
   "La botte change aussitôt de forme pour s'adapter parfaitement à votre pied."
!82
   "Une odeur nauséabonde de chaussettes sales."
!83
   "Le troll de pierre est sur votre passage."
!84
   "C'est une activité idéale pour jouer sur la plage."
!85
   "Vous remplissez la botte de sable, pour ensuite la vider dans le lac."
!86
   "Vous remplissez la botte avec l'eau du lac, pour ensuite la vider dans le sable."
!87
   "Le nuage se gonfle comme un bœuf, et d'un souffle vous éjecte de la cime de l'arbre."
!88
   "C'est la mémoire de l'Ancien Monde, une tempête de fer dans un corps de velours. Il veille sur le nid comme une nounou étatique."
!89
   "La force du vent aura raison, tôt ou tard, de cette branche bientôt morte."
!90
   "Son doux parfum sucré vous fait saliver."
!91
   "Sa peau rouge-orangée protège une chair, à coup sûr, enchantée ou empoisonnée."
!92
   "La pomme n'étant ni enchantée ni empoisonnée, vous recouvrez assez de force pour gravir à nouveau les différents étages de la tour du Corbeau."
!93
   "Le poisson se jette sur vous comme une mouche enragée. Dépité(e), vous regagner la rive du lac."
!94
   "Splash ! Le poisson bondit hors de l'eau et retombe en claquant la surface."
!95
   "Il virevolte, tournoie, zigzague autour de vous et vous échappe sans cesse."
!96
   "D'un geste vif et précis, vous plongez la botte dans l'eau et capturez le poisson."
!97
   "Le poisson tombe de la botte et retourne jouer dans le lac."
!98
   "Vous devez d'abord VIDER la botte avant de pouvoir traverser le lac."
!99
   "Le poisson tombe raide mort sur le sol. Empreint(e) de remords et de culpabilité, vous jetez la botte hors du jeu."
!100
   "Vous devez d'abord la VIDER."
!101
   "^La botte est emportée par le courant."
!102
   "Il est dangereux de se déplacer dans le noir, vous pourriez vous blesser."
!103
   "Une odeur de musque et d'ammoniaque."
!104
   "Vous aimeriez voir de nuit comme en plein jour, mais vous n'êtes qu'un être imparfait."
!105
   "Il ne faut pas moins de capacité pour aller jusqu'au néant que jusqu'au tout. Et vous n'avez plus ni force ni courage."
!106
   "Laissez cela aux oubliettes."
!107
   "L'obscurité n'en ferait qu'une bouchée."
!108
   "Le poisson s'entraîne à tenir en apnée au fond de la botte. Il espère ainsi améliorer ses performances."
!109
   "Utilisez le verbe EXAMINER ou CREUSER selon vos intentions."
!110
   "Un collier de perles brille dans la pénombre d'un autoportrait sans bouche voué au silence."
!111
   "^Alors que la tour s'effondre, vous vous jetez dans l'enfer des flammes et dégringolez les escaliers encore et encore."
!112
   "Le corbeau entre dans la pièce... l'ombre de ses ailes légères et silencieuses se projette au-dessus de vous, puis disparaît. Il a reconnu la voix de son maître, mais n'a pas trouvé l'offrande qu'exige, en contrepartie, son sacrifice."
!113
   "Le corbeau entre dans la pièce... Il sait ce qu'il doit faire, mais il n'aime pas ça. Assis sur le trône, vous êtes maintenant son maître : il doit obéir. Le corbeau se pose sur la torche enflammée et s'embrase instantanément. Le mur du fond commence à tourner, vous entraînant avec lui..."
!114
   "Ce n'est pas ce que la boîte aux lettres veut."
!115
   "L'outil idéal pour patouiller dans le sable."
!116
   "^La perle flotte à la dérive comme un bouchon à grand cru."
!117
   "Toc toc ! Il y a quelqu'un ?"
!118
   "Il s'est blotti au fin fond de la botte."
!119
   "Le nuage se gonfle comme un bœuf, et d'un souffle vous fait dégringoler les escaliers encore et encore."
!120
   "Il semble attendre quelqu'un."
!121
   "Une torche de résine ordinaire ayant appartenu, jadis, à un cracheur de feu."
!122
   "Le coffre est vide. Il semblerait que quelqu'un soit passé avant vous."
!123
   "Ce n'est pas ce que le coffre veut."
!124
   "Le coffre se referme aussitôt sur son bien le plus précieux. La perle est désormais en sécurité."
!125
   "Vous ne l'avez pas avec vous."
!126
   "Le passage est trop étroit pour pouvoir transporter la pelle et la torche en même temps."
!127
   "Vous pénétrez dans le passage obscur, et avancez prudemment sans savoir où aller. Vous remarquez une lumière quelque part par là-bas..."
!128
   "La torche s'écrie LUMOS ! et s'enflamme aussitôt. Elle connaît le chemin et vous guide à travers de longues galeries souterraines jusqu'à une lumière éblouissante..."
!129
   "Vous prenez la clé avec vous."
!130
   "^Vous rajoutez une étoile au firmament en lançant la torche vers le ciel."
!131
   "À propos de quoi ? À propos d'amour.^^Parlez-lui d'amour^(c) Auraes, 2019-2023^Licence CC BY-NC-ND 4.0^^Ce jeu est la traduction et l'adaptation remaniée de Talk to Him About Love crée pour l'Adventuron CaveJam de septembre 2019. Le code de cette version a pour contrainte d'utiliser, à une exception près, le jeu d'instruction du logiciel The Quill (Gilsoft).^Merci à Jean Pascal, Blaise Fontaine et autres inconnu(e)s.^"
!132
   "Le rat s'en saisit, se délecte du cœur de chair restant, puis croque à belles dents les pépins gorgés d'une potion de Sommeil. Aussitôt endormit, vous le prenez avec vous."
!133
   "Vous n'obtenez pour seule réponse que le bruissement léger du vent dans le feuillage."
!134
   "Par-delà, c'est l'au-delà."
!135
   "Ici, les portes se referment mais ne se rouvrent jamais."
!136
   "Vous entrouvrez la porte, jetez un œil à l'extérieur et la refermez aussitôt.^^Hé ! la porte n'aurait jamais dû s'ouvrir. C'est probablement un bogue : vous le signalez illico presto."
!137
   "La porte n'a ni poignée ni serrure."
!138
   "C'est une lourde et grande porte en bois massif sans poignée ni serrure."
!139
   "Il roupille tranquillement d'un sommeil sans rêves."
!140
   "Le corbeau jette un œil à l'offrande, mais ne la trouve pas assez charogne à son goût."
!141
   "Votre empathie naturelle pour les animaux vous l'interdit."
!142
   "D'un geste sûr et sans haine, vous tuez le rat et le jetez nonchalamment à vos pieds."
!143
   "Son corps appartient désormais à la mort et aux charognards."
!144
   "Le nuage se gonfle comme un bœuf, et d'un souffle vous éjecte dans les escaliers où vous dégringolez encore et encore."
!145
   "L'Oracle du destin semble hésiter. Doit-il vous tuer ou vous épargner ? Temps mort. Il cherche le game designer partout, mais ne le trouve nulle part.^^Alea jacta est, le score en est jeté..."
!146
   "Pff pff !"
!147
   "Plouf ! Vous faites un plongeon simple, somme toute assez médiocre."
!148
   "Deux yeux perçants, vifs et brillants, vous observent."
!149
   "La date de péremption est dépassée."
!150
   "C'est plein de pépins toxiques à mâchouiller sans modération."
!151
   "Elle est solidement fixée au mur et n'en bougera pas."
!152
   "La torche s'écrie NOX ! et s'éteint aussitôt. Les ombres se jettent sur vous et vous étreignent... le trône disparaît subitement entraîné par le mur tournant. La torche, percevant votre désarroi, remet un peu d'ordre avant de rallumer sa flamme."
!153
   "La torche se consume lentement d'une flamme sans ombre ni fumée. Elle est équipée d'un mécanisme à levier."
!154
   "Il est orné d'un motif sculpté, représentant un corbeau perché sur l'épaule d'un roi assis sur son trône. Ils se parlent."
!155
   "Il est solidement fixé au mur auquel il est adossé et n'en bougera pas."
!156
   "La gargouille étire son long cou et sa bouche béante au-dessus du lac."
!157
   "Haut perchée, la gargouille est hors d'atteinte."
!158
   "Haut perché, le corbeau est hors d'atteinte."
!159
   "^La pelle en profite pour se faire la belle."
!160
   "Il ne reconnaît que le chant puissant et rauque de sa mère."
!161
   "À ces mots, le corbeau ouvre un large bec et s'exclame :"
!162
   "Un regard inquisiteur transperce son plumage bleu-noir ; il ne vous quitte pas des yeux. Qu'attendez-vous de lui ?"
!163
   "Gare à vos doigts si vous recommencez !"
!164
   "Le Paradis est fermé jusqu'à nouvel ordre."
!165
   "L'Enfer est fermé jusqu'à nouvel ordre."
!166
   "C'est une voie sans issue."
!167
   "Moins risqué que la roulette russe ou le pile ou face, un LANCER de d100 devrait égayer votre destinée. Ou pas."
!168
   "Alors que vous vous asseyez sur le trône du Corbeau, de mauvaises pensées vous submergent."
!169
   "Travaillez donc à bien penser."
!170
   "Vous profitez du calme et de l'obscurité pour faire une petite sieste bien méritée."
!171
   "L'esprit maléfique du trône perturbe vos sentiments et vos pensées. La violence et la terreur vous submergent à nouveau."
!172
   "Fixée au mur opposé, la torche est hors d'atteinte."
!173
   "Le tableau est solidement fixé au mur et ne dissimule aucun trésor caché."
!174
   "Il manque une perle à la représentation picturale du collier."
!175
   "Vous n'obtenez aucune réponse."
!176
   "Vous traversez le lac comme une vague déferlante..."
!177
   "Vous refermez le coffre qui se rouvre aussitôt."
!178
   "Le coffre est irrémédiablement fermé et il le restera."
!179
   "Vous ne voyez rien que le soleil qui poudroie et l'herbe qui verdoie."
!180
   "Après avoir poussé un long cri primal de colère et de douleur, la porte finit par céder..."
!181
   "Le papillon vous précède et vous guide à travers le dédale d'impasses, de fausses pistes et de pièges dont recèle la forêt."
!182
   "Vous ouvrez grand la bouche et enfournez la bête. Bien qu'il soit garanti sans arêtes, vous recrachez quelque chose de consistant et brillant."
!183
   "Une perle de collier opaque ou la lumière pénètre et n'en ressort jamais."
!184
   "La torche est fée et n'obéira qu'à elle-même."
!185
   "L'assurance a imposé que le coffre soit scellé à son socle."
!186
   "Vous pourriez le blesser avec vos gros doigts maladroits."
!187
   "L'espèce est protégée par nécessité de préservation du patrimoine génétique."
!188
   "Une odeur pestilentielle de cadavre abandonné."
!189
   "Ça va, vous n'êtes pas blessé(e) ?"
!190
   "Une menotte de sûreté à mot de passe qui a dû enchaîner toutes sortes de créatures."
!191
   "N'espérez pas le ranimé d'un simple bouche-à-bouche."
!192
   "Homo sapiens anonyme ayant souhaité faire valoir son droit à l'oubli."
!193
   "La menotte n'était pas verrouillée ! Le squelette se disloque, puis tombe en poussière à vos pieds."
!194
   "Une vie entière réduite à un tas de poussière."
!195
   "Pour des raisons d'hygiène élémentaires et de potentiels germes pathogènes, vous préférez ne pas y toucher."
!196
   "Regardez bien, ma sœur ; est-ce assez ? dites-moi ; n'y suis-je point encore ? Vous vous enflez si fort que vous balayez d'un souffle cette vie de poussière."
!197
   "La menotte est scellée au mur et n'en bougera pas."
!198
   "C'est une carte topographique du Nouveau Monde. Vous devriez la LIRE."
!199
   "Une croix manuscrite, cerclée de rouge, souille l'extrémité ouest de la carte partiellement brûlée. Vous connaissez cet endroit."
!200
   "La carte s'embrase aux flammes du cachot. La croix sanglante vire au noir, se déploie... jusqu'à devenir un immense trou sans fond qui vous engloutit sans l'ombre d'une hésitation."
!201
   "Le mur de feu vous bloque le passage."
!202
   "La tour du Corbeau n'est plus que ruine et désolation."
!203
   "Vous creusez de-ci de-là, par-ci par-là, de ce côté-ci et de ce côté-là sans savoir où chercher."
!204
   "Vous n'avez ni pelleteuse ni marteau-piqueur avec vous."
!205
   "Vous creusez le sable en profondeur, découvrant un gros coquillage biscornu. Vous jetez la pelle cassée dans la poubelle des temps futurs."
!206
   "Pendant ce temps ...^"
!207
   "L'ombre du corbeau survole les lieux que vous avez visités à la recherche de quelque chose qu'il ne peut trouver."
!208
   "L'ombre du corbeau survole les lieux que vous avez visités. Il plonge subitement, freine en un vol plané, se laisse tomber puis s'envole à nouveau avec sa prise dans l'une de ses serres.^^La perle est à lui maintenant !"
!209
   "Vous farfouillez dans les entrailles en décomposition de la bête, et découvrez quelque chose de consistant et brillant que vous récupérez. Soucieux de ne pas gaspiller, vous mangez les restes."
!210
   "Le rat se réveille en sursaut, puis se rendort aussitôt."
!211
   "Ce n'est pas le moment de dormir ! Vous avez fort à faire."
!212
   "^La clé coule à pic. Un lutin facétieux qui faisait la planche, plonge à sa poursuite et la récupère."
!213
   "Vous découvrez un raccourci non cartographié ! Standing ovation pour le level designer."
!214
   "« Finite Incantatem ! »"
!215
   "Le Triton conque entonne une élégie funèbre à la mémoire de la tour du Corbeau."
!216
   "Le Triton conque se met à hurler faisant trembler toute la forêt."
!217
   "Un bébé oiseau tombe du nid et s'écrase au sol comme un fruit trop mûr."
!218
   "Une odeur de renfermé."
!219
   "Le gros ventre en spirale de ce coquillage doit sonner comme une corne de brume."
!220
   "Il est vide. Son habitant a été mis à la porte ou mangé."
!221
   "Préférez-vous du vin, du champagne ou un spiritueux ?"
!222
   "Le Triton conque se met à gémir. La souche creuse résonne comme un tambour fou meurtri de l'intérieur."
!223
   "Le Triton conque se met à vibrer. Les ondes sonores amplifiées par les parois de la grotte font vaciller le troll."
!224
   "Le Triton conque commence à chanter ..."
!225
   "D'abord douce et mélodieuse, la voix devient de plus en plus puissante. La camisole de pierre qui emprisonne le troll commence à se fendre, puis éclate soudainement. Le troll titube, grogne, se cogne, rit et finit par rentrer dans sa grotte où il s'allonge et se met à ronfler bruyamment.^^Il a une lettre cachée dans ses gros poings serrés."
!226
   "La partie est maintenant terminée. Merci d'avoir joué."
!227
   "Vous pénétrez à nouveau dans la grotte enchantée. Vous jetez un dernier regard au troll endormi tout en vous dirigeant vers le coffre. Il vous a reconnu et s'ouvre aussitôt. Vous récupérez la perle, déposez en échange le Triton conque et retournez dans la forêt où vous attend le lutin facétieux. Il vous prend la main et vous emmène au sommet de la montagne, puis disparaît."
!228
   "Tout est calme, silencieux. Un vent léger dissipe les lambeaux de brume agrippés aux cimes de la forêt ; le paysage s'ouvre à l'au-delà. Vous êtes assis(e) au bord du vide, le regard porté vers l'horizon.^^Chahutée par les courants du ciel, une ombre massive, percée d'un point de lumière vif et violant, s'enfuit à tire-d'aile en direction du large.^^Peut-être que cette histoire aurait pu se terminer autrement."
!229
   "Tout est calme, silencieux. Un vent léger dissipe les lambeaux de brume agrippés aux cimes de la forêt ; le paysage s'ouvre à l'au-delà. La perle s'agite, s'impatiente, déchire sa pellicule opaque, s'illumine d'une lumière vive et violente, puis s'échappe.^^Vous prenez de l'élan, courrez à perdre haleine et plongez, à sa suite, dans le vide. L'étoffe bleu-noir de vos ailes se déploie et vous prenez le large.^^Vous êtes le roi des corbeaux."
!230
   "Il est écrit : F=Gm1m2/r2"
!231
   "C'est d'abord un simple murmure, un parler à voix basse, distant, suave et flatteur dans une langue que vous comprenez. Le chuchotement se fait voix et tente brusquement de prendre possession de votre corps endormi..."
!232
   "Vous lui flanquez une fessée bien méritée."
!233
   "Vous caresser le troll de la main droite pour vous porter chance."
!234
   "Le troll se métamorphose en crapaud, puis redevient troll aussitôt."
!235
   "Un antre écoresponsable dépourvu de toutes commodités."
!236
   ""
!237
   "Tapez CODE suivit du mot de passe :"
!238
   "Mot de passe erroné !"
!239
   ""
!240
   "Le squelette se disloque, puis tombe en poussière à vos pieds."
!241
   "Une forêt sombre et profonde, aux confins de l'Ancien Monde, où suffoquent quelques clairières délaissées."
!242
   "Oui, vous êtes coupable. Disons-le d'emblée. C'est pourquoi vous fûtes attiré(e) ici."
!243
   "Le poisson abandonné à pourri. Au milieu des intestins décomposés, quelque chose brille intensément." !TODO
!244
   "Vous ravivez la flamme histoire de chauffer un peu l'ambiance."
!245
   "La torche fait office de levier à TIRER ou POUSSER selon les besoins."
!246
   "Le corbeau entre dans la pièce... l'ombre de ses ailes légères et silencieuses se projette au-dessus de vous. Constatant l'absence de son maître, il repart aussitôt."
!247
   "Le mur de feu dégage une importante chaleur infligeant 2d4 points de dégâts de feu à toutes les créatures situées à 3 mètres ou moins des flammes, et 1d4 points à celles se trouvant entre 3 et 6 mètres de distance."
!248
   "^Le poisson mort boit la tasse et flotte vers le rivage le ventre tourné vers le haut."
!249
   "Le rat s'agite, se réveille, vous fixe les yeux emplis d'effroi, puis se rendort aussitôt."
!250
   "Il dort d'un sommeil agité par l'imminence de sa propre mort."
!251
   "Vous pénétrez à nouveau dans la grotte enchantée. Vous jetez un dernier regard au troll endormi tout en vous dirigeant vers le coffre grand ouvert. Vous y déposez le Triton conque et retournez dans la forêt où vous attend le lutin facétieux. Il vous prend la main et vous emmène au sommet de la montagne, puis disparaît."
!252
   "À défaut de dé, vous lancez un pavé dans la mare."
!253
   "Vous devriez REGARDER à travers."
; !End_Array

