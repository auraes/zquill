Constant START_ROOM Prologue;

Object Prologue
with
   describe "Ce matin, juste avant le lever du soleil, le troll est rentré chez lui pour dormir. Comme d'habitude, avant de se coucher, il a jeté un coup d'œil à sa boîte aux lettres. Il y avait un mot d'amour.^Le troll ne sait pas lire, mais il a dans son trésor une perle qui connaît les langues de l'ensemble des êtres et des créatures.^^Le roi des corbeaux n'apprécie pas l'amour. Il veut la perle pour espionner et contrôler les âmes et les cœurs. Il demande au soleil de prendre la place de la lune et le soleil obéit.^^Les trolls détestent la lumière. Le soir, lorsqu'il sort de l'obscurité de sa grotte, surpris par la lumière éblouissante, le troll se transforme en pierre."
;

Object Prologue_suite
with
   describe "^Parlez-lui d'amour^Un jeu d'aventure textuel.^(c) Auraes, 2019-2023^Tapez AIDE pour plus d'informations.^",
   picture 13
;

Object Bord_lac
with
   describe "Vous êtes au bord d'un lac bordé de sable fin et d'arbres silencieux. L'orée de la forêt est à l'est.",
   picture 14,
   exits 'est' Foret
;

Object Foret
with
   describe "Vous êtes sur le chemin de la forêt. Deux grands arbres vous dominent de chaque côté. Un escalier se hisse au-delà d'une trouée lumineuse. La clairière est au sud, le lac à l'ouest.",
   picture 20,
   exits 'nord' Devant_grotte 'sud' Clairiere 'ouest' Bord_lac 'monter' Devant_grotte
;

Object Dans_arbre
with
   describe "Vous êtes à mi-hauteur de l'arbre au nid haut perché.",
   picture 16,
   exits 'descendre' Foret 'monter' Sommet_arbre 'grimper' Sommet_arbre
;

Object Dans_arbre_pomme
with
   describe "La pomme est là, à portée de main. La branche sur laquelle vous êtes perché(e) ne semble pas très solide.",
   picture 15,
   exits 'descendre' Foret 'monter' Sommet_arbre 'grimper' Sommet_arbre
;

Object Sommet_arbre
with
   describe "Vous êtes au sommet de l'arbre où repose, sous l'œil vigilant d'un nuage menaçant, un petit nid d'amour et de haine.",
   picture 12
;

Object Clairiere
with
   describe "Vous êtes au centre d'une petite clairière recouverte d'une herbe grasse et généreuse. Le chemin du retour est au nord.",
   picture 34,
   exits 'nord' Foret
;

Object Devant_grotte
with
   describe "Vous êtes à l'extérieur de la grotte enchantée. Un troll de pierre en bloque l'accès. Vous pouvez redescendre en empruntant l'escalier.",
   picture 11
;

Object Dans_lac
with
   describe "Vous coulez, encore et encore.^^Votre botte est emportée par le courant.",
   picture 3
;

Object Fond_lac
with
   describe "Vous êtes au fond du lac, mort(e) à 97 %. Le level designer réfléchit, veuillez patienter...",
   picture 1,
   exits 'nord' Fond_lac_98 'sud' Fond_lac_98 'est' Fond_lac_98 'ouest' Fond_lac_98
     'monter' Fond_lac_98 'descendre' Fond_lac_98 'nager' Fond_lac_98
;

Object Fond_lac_98
with
   describe "Vous êtes au fond du lac, mort(e) à 98 %. Le game designer réfléchit, veuillez patienter...",
   picture 3,
   exits 'nord' Fond_lac_99 'sud' Fond_lac_99 'est' Fond_lac_99 'ouest' Fond_lac_99
      'monter' Fond_lac_99 'descendre' Fond_lac_99 'nager' Fond_lac_99
;

Object Fond_lac_99
with
   describe "Vous êtes au fond du lac, mort(e) à 99 %. L'economic designer réfléchit, veuillez patienter...",
   picture 1,
   exits 'nord' Fond_lac1 'sud' Fond_lac1 'est' Fond_lac1 'ouest' Fond_lac1
      'monter' Fond_lac1 'descendre' Fond_lac1 'nager' Fond_lac1
;

Object Fond_lac1
with
   describe "Un poisson s'approche de vous et essaie de vous dire quelque chose que vous ne comprenez pas.^^Puis soudain, un immense cri de détresse retentit dans le ciel... quelque chose d'incroyablement brillant vient de tomber dans le lac.",
   picture 3
;

Object Fond_lac2
with
   describe "Le poisson s'en approche, tourne un peu autour, l'avale subitement puis revient vers vous et vous dit : Es-tu mort(e) ? Sinon, laisse-moi te dire un secret : maintenant tu sais nager et grimper.^^Le poisson disparaît dans les eaux profondes et vous regagnez à la nage la rive du lac.",
   picture 2,
   !go TODO
;

Object Dans_grotte
with
   describe "Vous êtes à l'intérieur de la grotte enchantée. L'aménagement de l'antre est sommaire et sans commodités. La sortie est au nord et l'entrée d'un boyau souterrain à l'est.",
   picture 18
;

Object Exterieur_tour
with
   describe "Vous traversez le lac comme une vague déferlante...",
   picture 8
;

Object Sommet_tour
with
   describe "Vous êtes au sommet de la tour du Corbeau. Alors qu'un nuage reprend son souffle avant l'orage, un corbeau perché sur une gargouille vous observe. Vous pouvez redescendre en empruntant l'escalier.",
   picture 4,
   exits 'descendre' Etage_trone
;

Object Etage_trone
with
   describe "Vous êtes dans la salle du trône. Les ombres projetées par la lumière d'une torche dansent autour de vous. Vous pouvez poursuivre votre ascension ou redescendre vers le rez-de-chaussée.",
   picture 5,
   exits 'descendre' Interieur_tour
;

Object Sur_trone
with
   describe "Vous êtes assis sur le trône du Corbeau. Un portrait peint attire votre attention.^^La flamme d'une torche scintille sur le mur opposé.",
   picture 7,
   exits 'lever' Etage_trone 'sortir' Etage_trone
;

Object Interieur_tour
with
   describe "Vous êtes au rez-de-chaussée de la tour du Corbeau, adossé(e) à la porte qui vous a piégé(e). Vous pouvez utiliser les escaliers pour monter et descendre, à pied, les 1001 marches de la tour.^^Une petite lumière scintille en haut de l'escalier.",
   picture 39,
   exits 'monter' Etage_trone
;

Object Etage_1
with
   describe "Vous êtes au niveau inférieur 1 de la tour du Corbeau.^^« JE CHANTERAI À LA GLOIRE DE TON NOM ;",
   picture 31,
   exits 'monter' Interieur_tour 'descendre' Etage_2
;

Object Etage_2
with
   describe "Vous êtes au niveau inférieur 2 de la tour du Corbeau.^^« JE DÉTERRERAI POUR TOI LES TRÉSORS DE LA TERRE ;",
   picture 32,
   exits 'monter' Interieur_tour 'descendre' Etage_3
;

Object Etage_3
with
   describe "Vous êtes au niveau inférieur 3 de la tour du Corbeau.^^« J'OFFRIRAI MA CHAIR À TES PLAISIRS ;",
   picture 31,
   exits 'monter' Interieur_tour 'descendre' Etage_4
;

Object Etage_4
with
   describe "Vous êtes au niveau inférieur 4 de la tour du Corbeau.^^« JE T'ACCORDERAI TROIS VŒUX ;",
   picture 32,
   exits 'monter' Interieur_tour 'descendre' Etage_5
;

Object Etage_5
with
   describe "Vous êtes au niveau inférieur 5 de la tour du Corbeau.^^« J'APAISERAI TA FAIM ET TA SOIF ;",
   picture 31,
   exits 'monter' Interieur_tour 'descendre' Etage_6
;

Object Etage_6
with
   describe "Vous êtes au niveau inférieur 6 de la tour du Corbeau.^^« ENRAGÉE DE ME VOIR SI LONGTEMPS PRISONNIÈRE...",
   picture 32,
   exits 'monter' Interieur_tour 'descendre' Etage_7
;

Object Etage_7
with
   describe "Vous êtes au niveau inférieur 7 de la tour du Corbeau.^^« J'EFFACERAI TON NOM ;",
   picture 31,
   exits 'monter' Interieur_tour 'descendre' Etage_8
;

Object Etage_8
with
   describe "Vous êtes au niveau inférieur 8 de la tour du Corbeau.^^« JE VOLERAI TES BIENS ;",
   picture 32,
   exits 'monter' Interieur_tour 'descendre' Etage_9
;

Object Etage_9
with
   describe "Vous êtes au niveau inférieur 9 de la tour du Corbeau.^^« JE TE POSSÉDERAI ;",
   picture 31,
   exits 'monter' Interieur_tour 'descendre' Etage_10
;

Object Etage_10
with
   describe "Vous êtes au niveau inférieur 10 de la tour du Corbeau.^^« JE BRISERAI TA DESTINÉE ;",
   picture 32,
   exits 'monter' Interieur_tour 'descendre' Etage_11
;

Object Etage_11
with
   describe "Vous êtes au niveau inférieur 11 de la tour du Corbeau.^^« JE TE TUERAI...",
   picture 31,
   exits 'monter' Interieur_tour 'descendre' Etage_12
;

Object Etage_12
with
   describe "Vous êtes au niveau inférieur 12 de la tour du Corbeau.^^  MAIS JE TE LAISSERAI CHOISIR TA MORT. »",
   picture 32,
   exits 'monter' Interieur_tour
;

Object Etage_13
with
   describe "Vous êtes au niveau inférieur 13 de la tour du Corbeau.^^Tentez votre chance à notre grand jeu concours !",
   picture 40,
   !go 'nord' PlaceVillage 'sortir' PlaceVillage
;

Object Obscurite_cachot
with
   describe "Vous êtes dans l'obscurité. Rien n'est visible.",
   picture 30
;

Object Cachot
with
   describe "Vous êtes au niveau inférieur 1001 de la tour du Corbeau.^^Le mur ouest est dévoré par le feu.",
   picture 6
;

Object Epilogue
with
   describe "Merci d'avoir joué.^",
   picture 19
;

Object Fake_room1
with
   describe "Un corbeau s'enfuit de la grotte avec quelque chose de brillant dans les serres.",
   picture 10
;

Object Fake_room2
with
   describe "Tapis dans l'ombre, un rat hostile et affamé attend sa part.",
   picture 38
;

Object Fake_room3
with
   describe "D'innombrables papillons s'échappent en volutes désordonnées du sinistre cachot.",
   picture 36
;

Object Fake_room4
with
   describe "« Nox ! »",
   picture 6
;

Object Fake_room5
with
   describe "« Lumos ! »",
   picture 30
;

