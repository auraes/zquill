Constant FRENCH;
Constant CENTERING_SYSMESS_16 24;
Constant PROMPT ">";
Constant YES 'o';
Constant INK_DEFAULT CLR_WHITE;
Constant PAPER_DEFAULT CLR_BLACK;

#Ifdef FRENCH;
! treize emplacements disponibles ; il manque 'ä' 'ü' 'ÿ' 'ö' 'æ'
Zcharacter 'à';
Zcharacter 'â';
Zcharacter 'ç';
Zcharacter 'è';
Zcharacter 'é';
Zcharacter 'ê';
Zcharacter 'ë';
Zcharacter 'î';
Zcharacter 'ï';
Zcharacter 'ô';
Zcharacter 'œ';
Zcharacter 'ù';
Zcharacter 'û';
#Endif;

Array sysmess-->
!0
   "Vous êtes dans l'obscurité. Rien n'est visible.^"
!1
   "^Il y a aussi :^"
!2
   "^Que faites-vous ?^"
!3
   "^Que faites-vous ?^"
!4
   "^Que faites-vous ?^"
!5
   "^Que faites-vous ?^"
!6
   "Je n'ai pas compris votre commande.^"
!7
   "Vous ne pouvez pas aller par là.^"
!8
   "Vous ne pouvez pas faire ça.^"
!9
   "Vous avez :^"
!10
   " (sur vous)"
!11
   "Rien du tout.^"
!12
   "Voulez-vous vraiment interrompre la partie en cours ?^"
!13
   "^Fin.^^Vous voulez rejouer ?^"
!14
   "Au revoir.^"
!15
   "OK.^" ! O.K.
!16
   "(Appuyez sur une touche)"
!17
   "Vous terminez la partie en "
!18
   " tour"
!19
   "s"
!20
   ".^"
!21
   "Votre score est de "
!22
   " sur 100.^"
!23
   "Vous ne l'avez pas sur vous.^"
!24
   "Vous ne pouvez rien transporter de plus.^"
!25
   "Vous l'avez déjà avec vous.^"
!26
   "Elle n'est pas ici.^"
!27
   "Vous ne pouvez rien transporter de plus.^"
!28
   "Vous ne l'avez pas avec vous.^"
!29
   "Vous l'avez déjà sur vous.^"
!30
   "O^"
!31
   "N^"
!32
   "La sauvegarde a échoué.^"
!33
   "Le chargement de la sauvegarde a échoué.^"
!34
   "^"
!35
   "Vous ne les avez pas sur vous.^"
!36
   "Vous les avez déjà avec vous.^"
!37
   "Il n'est pas ici.^"
!38
   "Elles ne sont pas ici.^"
!39
   "Ils ne sont pas ici.^"
!40
   "Vous ne les avez pas avec vous.^"
!41
   "Vous les avez déjà sur vous.^"
; !End_Array

